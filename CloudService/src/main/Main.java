package main;

import static spark.Spark.*;

import java.awt.Window.Type;
import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

import javaxt.utils.Array;
import model.Aktivnost;
import model.DatabaseManager;
import model.Disk;
import model.DiskTip;
import model.Kategorija;
import model.Korisnici;
import model.Korisnik;
import model.Organizacija;
import model.Uloga;
import model.VirtuelnaMasina;
import spark.Request;
import spark.Session;
import spark.utils.IOUtils;
import util.ActInfo;
import util.CategoryInfo;
import util.DriveInfo;
import util.FiltersInfo;
import util.LoginInfo;
import util.Receipt;
import util.UserInfo;
import util.VmAddInfo;
import util.VMPrice;
import util.DrivePrice;
public class Main {

	public static Gson g = new Gson();
	public static DatabaseManager dbm = new DatabaseManager();
	public static Korisnici users = new Korisnici();
	// server salje: 2020-01-16T22:01 => 16.01.2020 22:01

	public static void SacuvajKorisnike() {
		String PATH = "./files/korisnici.json";
		try {
			Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			FileWriter fwr = new FileWriter(PATH);
			g.toJson(users.getSviKorisnici(), fwr);
			fwr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		port(4567);

		staticFiles.externalLocation(new File("./static/").getCanonicalPath());

		get("/test", (req, res) -> {
			return "Works";
		});

		post("/main/login", (req, res) -> {
			String payload = req.body();
			LoginInfo login = g.fromJson(payload, LoginInfo.class);
			Korisnik korisnik = users.nadjiKorisnika(login.email);
			if (korisnik != null) {
				if (korisnik.getLozinka().equals(login.password)) {
					getKorisnik(req);
					return ("SUCCESS");

				}
			}
			return ("NOT_FOUND");
		});

		get("/main/org/getOrganizacije", (req, res) -> {
			Korisnik user = getKorisnik(req);
			if (user.getUloga() == Uloga.SUPER_ADMIN) {
				return g.toJson(dbm.getOrganizacije());
			} else if (user.getUloga() == Uloga.ADMIN) {
				ArrayList<Organizacija> org = new ArrayList<Organizacija>();
				for (Organizacija o : dbm.getOrganizacije()) {
					if (user.getOrganizacija().equalsIgnoreCase(o.getIme())) {
						org.add(o);
						return g.toJson(org);
					}
				}

			} else {
				res.status(403);
			}
			return g.toJson(new ArrayList<Organizacija>());

		});

		get("/main/org/getVMasine", (req, res) -> {
			Korisnik user = getKorisnik(req);
			if (user.getUloga() == Uloga.SUPER_ADMIN) {
				return g.toJson(dbm.getvMasine());
			} else {
				for (Organizacija o : dbm.getOrganizacije()) {
					if (user.getOrganizacija().equalsIgnoreCase(o.getIme())) {
						return g.toJson(o.getVirtuelneMasine());
					}
				}

			}
			return g.toJson(new ArrayList<VirtuelnaMasina>());

		});
		// SAD SE VRACAJU VM A NE ORGANIZACIJE
		post("/main/org/search", (req, res) -> {

			ArrayList<VirtuelnaMasina> filtered = new ArrayList<VirtuelnaMasina>();

			FiltersInfo values = g.fromJson(req.body(), FiltersInfo.class);
			Korisnik k = getKorisnik(req);
			if (values.CPU_do == null || values.CPU_do.equals(""))
				values.CPU_do = String.valueOf(Integer.MAX_VALUE);
			if (values.CPU_od == null || values.CPU_od.equals(""))
				values.CPU_od = String.valueOf(Integer.MIN_VALUE);
			if (values.GPU_do == null || values.GPU_do.equals(""))
				values.GPU_do = String.valueOf(Integer.MAX_VALUE);
			if (values.GPU_od == null || values.GPU_od.equals(""))
				values.GPU_od = String.valueOf(Integer.MIN_VALUE);
			if (values.RAM_do == null || values.RAM_do.equals(""))
				values.RAM_do = String.valueOf(Integer.MAX_VALUE);
			if (values.RAM_od == null || values.RAM_od.equals(""))
				values.RAM_od = String.valueOf(Integer.MIN_VALUE);
			if (values.vmName == null || values.vmName.equals(""))
				values.vmName = null;

			for (VirtuelnaMasina vm : dbm.getvMasine()) {
				if (values.vmName != null) {
					if (!values.vmName.equalsIgnoreCase(vm.getIme()))
						continue;
				}
				if (vm.getKategorija().getBrojJezgara() >= Integer.parseInt(values.CPU_od)
						&& vm.getKategorija().getBrojJezgara() <= Integer.parseInt(values.CPU_do)
						&& vm.getKategorija().getBrojGPUJezgara() >= Integer.parseInt(values.GPU_od)
						&& vm.getKategorija().getBrojGPUJezgara() <= Integer.parseInt(values.GPU_do)
						&& vm.getKategorija().getRAM() >= Integer.parseInt(values.RAM_od)
						&& vm.getKategorija().getRAM() <= Integer.parseInt(values.RAM_do)) {
					if (k.getUloga() == Uloga.SUPER_ADMIN) {
						filtered.add(vm);
					} else {
						if (k.getOrganizacija().equalsIgnoreCase(vm.getOrganizacija()))
							filtered.add(vm);
					}

				}

			}
			return g.toJson(filtered);
		});

		get("/main/logout", (req, res) -> {
			req.session().invalidate();
			return "";

		});

		get("/main/session/getUser", (req, res) -> {
			Korisnik curr = getKorisnik(req);
			return g.toJson(curr);

		});

		get("/main/org/getKategorije", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				return g.toJson(dbm.getKategorije());
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/addVm", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String payload = req.body();
				System.out.println(payload);
				VmAddInfo info = g.fromJson(payload, VmAddInfo.class);

				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (info.vm_ime.equalsIgnoreCase(vm.getIme())) {
						return "FAIL";
					}
				}

				VirtuelnaMasina add = new VirtuelnaMasina();
				add.setIme(info.vm_ime);

				for (Kategorija k : dbm.getKategorije()) {
					if (k.getIme().equals(info.kat)) {
						add.setKategorija(k);
						break;
					}
				}

				for (String string : info.drives) {
					for (Disk d : dbm.getDiskovi()) {
						if (d.getIme().equalsIgnoreCase(string)) {
							add.dodajDisk(d);
							d.setVirtuelnaMasina(add.getIme());
						}
					}
				}
				Organizacija selected = null;
				for (Organizacija o : dbm.getOrganizacije()) {
					if (o.getIme().equals(info.org)) {
						o.dodajVirtuelnuMasinu(add);
						dbm.getvMasine().add(add);
						add.setOrganizacija(o.getIme());
						selected = o;
						break;
					}
				}
				for (Disk d : selected.getDiskovi()) {
					for (String dr : info.drives) {
						if (d.getIme().equalsIgnoreCase(dr)) {
							d.setVirtuelnaMasina(info.vm_ime);
						}
					}
				}
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		get("/main/org/getVM/:id", (req, res) -> {

			String vmName = req.params("id");

			VirtuelnaMasina requested = null;

			for (VirtuelnaMasina vm : dbm.getvMasine()) {
				if (vm.getIme().equalsIgnoreCase(vmName)) {
					requested = vm;
					break;
				}
			}

			return g.toJson(requested);

		});

		get("/main/org/getORG/:id", (req, res) -> {

			String vmName = req.params("id");

			Organizacija requested = null;
			boolean found = false;
			for (Organizacija o : dbm.getOrganizacije()) {
				if (found)
					break;
				for (VirtuelnaMasina v : o.getVirtuelneMasine()) {
					if (v.getIme().equalsIgnoreCase(vmName)) {
						requested = o;
						found = true;
						break;
					}
				}
			}
			return g.toJson(requested);

		});

		get("/main/org/getVirtualMachines/:id", (req, res) -> {
			
			String id = req.params("id");
			
			for (Organizacija o : dbm.getOrganizacije()) {
				if(id.equalsIgnoreCase(o.getIme())) {
					return g.toJson(o.getVirtuelneMasine());
				}
			}
			return "FAILED";
		});
		
		post("/main/org/editVM/:id", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String payload = req.body();
				VmAddInfo info = g.fromJson(payload, VmAddInfo.class);
				String edited = req.params("id");

				Kategorija k = null;
				Organizacija o_new = null;
				Organizacija old = null;
				VirtuelnaMasina vm_old = null;
				ArrayList<Disk> diskovi_new = new ArrayList<Disk>();
				ArrayList<Disk> diskovi_old = new ArrayList<Disk>();
				
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (vm.getIme().equalsIgnoreCase(edited)) {
						vm_old = vm;
						break;
					}
				}

				diskovi_old = vm_old.getDiskovi();

				for (Kategorija kat : dbm.getKategorije()) {
					if (kat.getIme().equalsIgnoreCase(info.kat)) {
						k = kat;
						break;
					}
				}

				if (!info.vm_ime.equalsIgnoreCase(edited)) {
					for (VirtuelnaMasina vMas : dbm.getvMasine()) {
						if (vMas.getIme().equalsIgnoreCase(info.vm_ime))
							return "FAILED";
					}
				}
				for (Organizacija org : dbm.getOrganizacije()) {
					if (org.getVirtuelneMasine().contains(vm_old)) {
						old = org;
						break;
					}

				}

				dbm.getvMasine().remove(vm_old);

				old.getVirtuelneMasine().remove(vm_old);
				vm_old.setIme(info.vm_ime);
				vm_old.setKategorija(k);
				
				
				for(String dr : info.drives) {
					for(Disk d : dbm.getDiskovi()) {
						if(d.getIme().equalsIgnoreCase(dr)) {
							diskovi_new.add(d);
							d.setVirtuelnaMasina(vm_old.getIme());
						}
					}
				}
				
				if(diskovi_new.size() == 0) {
					for(Disk d : dbm.getDiskovi()) {
						if(diskovi_old.contains(d)) {
							d.setVirtuelnaMasina(null);
						}
					}
				}
				
				vm_old.setDiskovi(diskovi_new);
				
				for(Disk d : old.getDiskovi()) {
					if(diskovi_old.contains(d)) {
						d.setVirtuelnaMasina(null);
					}
					
					if(diskovi_new.contains(d)) {
						d.setVirtuelnaMasina(vm_old.getIme());
					}
				}
				
				old.getVirtuelneMasine().add(vm_old);

				dbm.getvMasine().add(vm_old);

				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/deleteVM/:id", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String del = req.params("id");
				VirtuelnaMasina delete = null;
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (vm.getIme().equalsIgnoreCase(del)) {
						delete = vm;
					}
				}

				dbm.getvMasine().remove(delete);
				for (Organizacija o : dbm.getOrganizacije()) {
					if(o.getIme().equalsIgnoreCase(delete.getOrganizacija())) {
						for(Disk d : delete.getDiskovi()) {
							if(o.getDiskovi().contains(d)) {
								d.setVirtuelnaMasina(null);
							}
						}
					}
					o.getVirtuelneMasine().remove(delete);
				}
				
				for(Disk d : dbm.getDiskovi()) {
					if(delete.getDiskovi().contains(d))
						d.setVirtuelnaMasina(null);
				}
				
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		get("/main/org/activeVM/:id", (req, res) -> {

			String vm = req.params("id");
			Date today = new Date(System.currentTimeMillis());
			
			for (VirtuelnaMasina v : dbm.getvMasine()) {
				if (v.getIme().equalsIgnoreCase(vm)) {
					for (Aktivnost a : v.getAktivnosti()) {
						if (a.getDatumGasenja() == null || (a.getDatumPaljenja().before(today) && a.getDatumGasenja().after(today))) {
							return "ON";
						}
					}
				}
			}

			return "OFF";

		});

		post("/main/org/switchVM/:id", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String sw = req.params("id");
				Date today = new Date(System.currentTimeMillis());
				
				
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (vm.getIme().equalsIgnoreCase(sw)) {
						for (Aktivnost a : vm.getAktivnosti()) {
							if (a.getDatumGasenja() == null || (a.getDatumPaljenja().before(today) && a.getDatumGasenja().after(today))) {
								a.setDatumGasenja(new Date());

								for (VirtuelnaMasina find : dbm.OrganizacijaZaVM(vm).getVirtuelneMasine()) {
									if (find.equals(vm))
										find.setAktivnosti(vm.getAktivnosti());
								}
								dbm.SacuvajPromene();
								return "SUCCESS";
							}
						}

						Aktivnost curr = new Aktivnost();
						curr.setDatumPaljenja(new Date());
						curr.setDatumGasenja(null);

						vm.dodajAktivnost(curr);
						for (VirtuelnaMasina find : dbm.OrganizacijaZaVM(vm).getVirtuelneMasine()) {
							if (find.equals(vm)) {
								find.setAktivnosti(vm.getAktivnosti());
							}

						}

						dbm.SacuvajPromene();
						return "SUCCESS";
					}
				}

			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
			return "FAILED";
		});

		post("/main/org/editSelf", (req, res) -> {
			String payload = req.body();
			UserInfo info = g.fromJson(payload, UserInfo.class);
			Korisnik loggedIn = getKorisnik(req);

			if (!loggedIn.getEmail().equalsIgnoreCase(info.email))
				if (users.getSviKorisnici().get(info.email) != null)
					return "FAILED";

			Korisnik old = null;
			Organizacija org = dbm.OrganizacijaZaKorisnika(loggedIn);

			old = users.getSviKorisnici().get(loggedIn.getEmail());
			if (org != null)
				org.getKorisnici().remove(old);
			users.obrisiKorisnika(loggedIn.getEmail());

			loggedIn.setEmail(info.email);
			loggedIn.setIme(info.name);
			loggedIn.setLozinka(info.password);
			loggedIn.setPrezime(info.lastname);

			users.dodajKorisnika(loggedIn);
			if (org != null)
				org.getKorisnici().add(loggedIn);

			SacuvajKorisnike();
			dbm.SacuvajPromene();
			return "SUCCESS";

		});

		post("/main/org/editUser/:id", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String payload = req.body();
				UserInfo info = g.fromJson(payload, UserInfo.class);
				String id = req.params("id");
				Korisnik edited = users.getSviKorisnici().get(id);

				if (!edited.getEmail().equalsIgnoreCase(info.email))
					if (users.getSviKorisnici().get(info.email) != null)
						return "FAILED";

				Korisnik old = null;
				Organizacija org = dbm.OrganizacijaZaKorisnika(edited);

				old = users.getSviKorisnici().get(edited.getEmail());
				if (org != null)
					org.getKorisnici().remove(old);
				users.obrisiKorisnika(edited.getEmail());

				edited.setEmail(info.email);
				edited.setIme(info.name);
				edited.setLozinka(info.password);
				edited.setPrezime(info.lastname);
				edited.setUloga(Uloga.valueOf(info.uloga.toUpperCase()));
				users.dodajKorisnika(edited);
				if (org != null)
					org.getKorisnici().add(edited);

				SacuvajKorisnike();
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/addUser", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String payload = req.body();

				UserInfo info = g.fromJson(payload, UserInfo.class);

				if (users.getSviKorisnici().get(info.email) != null) {
					return "FAILED";
				}
				System.out.println(info.uloga);
				Korisnik k = new Korisnik(info.email, info.password, info.name, info.lastname, info.organizacija,
						Uloga.valueOf(info.uloga.toUpperCase()));

				users.dodajKorisnika(k);

				for (Organizacija o : dbm.getOrganizacije()) {
					if (o.getIme().equalsIgnoreCase(info.organizacija)) {
						o.getKorisnici().add(k);
					}
				}

				SacuvajKorisnike();
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/deleteUser/:email", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String id = req.params("email");

				Organizacija org = null;

				for (Organizacija o : dbm.getOrganizacije()) {
					if (users.nadjiKorisnika(id).getOrganizacija().equalsIgnoreCase(o.getIme())) {
						org = o;
						break;
					}
				}
				org.obrisiKorisnika(id);
				users.obrisiKorisnika(id);

				SacuvajKorisnike();
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/deleteAct/:id/:dat", (req, res) -> {

			String date = req.params("dat");
			String vm = req.params("id");
			SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy h:mm:ss a");
			int act = -1;
			System.out.println("Evo ovde sam");
			System.out.println(date);
			System.out.println(vm);
			VirtuelnaMasina vmasina = null;

			for (VirtuelnaMasina virt : dbm.getvMasine()) {
				if (virt.getIme().equalsIgnoreCase(vm)) {
					vmasina = virt;
					for (Aktivnost a : virt.getAktivnosti()) {
						System.out.println(sdf.format(a.getDatumPaljenja()));
						if (sdf.format(a.getDatumPaljenja()).equalsIgnoreCase(date)) {
							act = virt.getAktivnosti().indexOf(a);
							break;
						}
					}
				}
			}

			vmasina.getAktivnosti().remove(act);

			Organizacija o = dbm.OrganizacijaZaVM(vmasina);
			Aktivnost k_ = null;
			for (VirtuelnaMasina x : o.getVirtuelneMasine()) {
				if (x.getIme().equalsIgnoreCase(vm)) {
					vmasina = x;
					for (Aktivnost a : vmasina.getAktivnosti()) {
						if (sdf.format(a.getDatumPaljenja()).equalsIgnoreCase(date)) {

							k_ = a;
							break;
						}
					}
				}
			}
			vmasina.getAktivnosti().remove(k_);
			dbm.SacuvajPromene();
			return "SUCCESS";

		});

		post("/main/org/addAct/:id", (req, res) -> {

			String vm = req.params("id");
			String payload = req.body();

			ActInfo info = g.fromJson(payload, ActInfo.class);

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

			Date dateP = formatter.parse(info.datumPaljenja);
			Date dateG = formatter.parse(info.datumGasenja);


			Organizacija o = null;
			for (VirtuelnaMasina vMas : dbm.getvMasine()) {
				if (vMas.getIme().equalsIgnoreCase(vm)) {
					for (Aktivnost act : vMas.getAktivnosti()) {
						if ((dateP.compareTo(act.getDatumPaljenja()) > 0 && dateP.compareTo(act.getDatumGasenja()) < 0)
								|| (dateG.compareTo(act.getDatumPaljenja()) > 0
										&& dateG.compareTo(act.getDatumGasenja()) < 0)
								|| (dateP.compareTo(act.getDatumPaljenja()) < 0
										&& dateG.compareTo(act.getDatumGasenja()) > 0)
								|| (dateP.compareTo(dateG)) >= 0) {
							return "FAILED";
						}

					}
					vMas.dodajAktivnost(new Aktivnost(dateP, dateG));
					o = dbm.OrganizacijaZaVM(vMas);
					break;
				}
			}

			for (VirtuelnaMasina virt : o.getVirtuelneMasine()) {
				if (virt.getIme().equalsIgnoreCase(vm)) {
					virt.dodajAktivnost(new Aktivnost(dateP, dateG));
					break;
				}
			}

			dbm.SacuvajPromene();
			return "SUCCESS";

		});

		get("/main/org/getUsers", (req, res) -> {

			Korisnik k = getKorisnik(req);

			if (k.getUloga() == Uloga.SUPER_ADMIN) {
				return g.toJson(users.getSviKorisnici().values());
			} else if (k.getUloga() == Uloga.ADMIN) {
				for (Organizacija org : dbm.getOrganizacije()) {
					if (org.getIme().equalsIgnoreCase(k.getOrganizacija())) {
						return g.toJson(org.getKorisnici());
					}
				}
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}

			return g.toJson(new ArrayList<Korisnik>());

		});

		get("/main/org/getUser/:user_id", (req, res) -> {

			String user = req.params("user_id");

			return g.toJson(users.nadjiKorisnika(user));

		});

		get("/main/org/getKategorija/:cat_id", (req, res) -> {

			String id = req.params("cat_id");

			for (Kategorija k : dbm.getKategorije()) {
				if (k.getIme().equalsIgnoreCase(id)) {
					return g.toJson(k);
				}
			}

			return g.toJson(new Kategorija());

		});
		post("/main/org/addCategory", (req, res) -> {
			if (getKorisnik(req).getUloga() == Uloga.SUPER_ADMIN) {
				String payload = req.body();

				CategoryInfo info = g.fromJson(payload, CategoryInfo.class);

				for (Kategorija k : dbm.getKategorije()) {
					if (k.getIme().equalsIgnoreCase(info.ime)) {
						return "FAILED";
					}
				}

				Kategorija newKat = new Kategorija(info.ime, Integer.parseInt(info.brojJezgara),
						Integer.parseInt(info.RAM), Integer.parseInt(info.brojGPUJezgara));

				dbm.getKategorije().add(newKat);

				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/editCategory/:cat_id", (req, res) -> {
			if (getKorisnik(req).getUloga() == Uloga.SUPER_ADMIN) {
				String payload = req.body();

				CategoryInfo info = g.fromJson(payload, CategoryInfo.class);
				String id = req.params("cat_id");
				for (Kategorija k : dbm.getKategorije()) {
					if (k.getIme().equalsIgnoreCase(info.ime) && !k.getIme().equalsIgnoreCase(id)) {
						return "FAILED";
					}
				}

				for (Kategorija k : dbm.getKategorije()) {
					if (k.getIme().equalsIgnoreCase(id)) {
						k.setIme(info.ime);
						k.setBrojJezgara(Integer.parseInt(info.brojJezgara));
						k.setBrojGPUJezgara(Integer.parseInt(info.brojGPUJezgara));
						k.setRAM(Integer.parseInt(info.RAM));
						break;
					}
				}

				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (vm.getKategorija().getIme().equalsIgnoreCase(id)) {
						vm.getKategorija().setIme(info.ime);
						vm.getKategorija().setBrojJezgara(Integer.parseInt(info.brojJezgara));
						vm.getKategorija().setBrojGPUJezgara(Integer.parseInt(info.brojGPUJezgara));
						vm.getKategorija().setRAM(Integer.parseInt(info.RAM));
						vm.setBrojGPUJezgara(vm.getKategorija().getBrojGPUJezgara());
						vm.setBrojJezgara(vm.getKategorija().getBrojJezgara());
						vm.setRAM(vm.getKategorija().getRAM());
					}
				}

				for (Organizacija org : dbm.getOrganizacije()) {
					for (VirtuelnaMasina vm : org.getVirtuelneMasine()) {
						if (vm.getKategorija().getIme().equalsIgnoreCase(id)) {
							vm.getKategorija().setIme(info.ime);
							vm.getKategorija().setBrojJezgara(Integer.parseInt(info.brojJezgara));
							vm.getKategorija().setBrojGPUJezgara(Integer.parseInt(info.brojGPUJezgara));
							vm.getKategorija().setRAM(Integer.parseInt(info.RAM));
							vm.setBrojGPUJezgara(vm.getKategorija().getBrojGPUJezgara());
							vm.setBrojJezgara(vm.getKategorija().getBrojJezgara());
							vm.setRAM(vm.getKategorija().getRAM());
						}
					}
				}

				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/deleteCategory/:cat_id", (req, res) -> {
			if (getKorisnik(req).getUloga() == Uloga.SUPER_ADMIN) {
				String id = req.params("cat_id");

				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (vm.getKategorija().getIme().equalsIgnoreCase(id)) {
						return "ATTACHED";
					}
				}
				Kategorija forDelete = null;

				for (Kategorija k : dbm.getKategorije()) {
					if (k.getIme().equalsIgnoreCase(id)) {
						forDelete = k;
						break;
					}
				}

				dbm.getKategorije().remove(forDelete);
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/addOrg", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));

				Part filePart = req.raw().getPart("i_file");
				String namePart = req.queryParams("name");
				String descPart = req.queryParams("desc");
				try (InputStream inputStream = filePart.getInputStream()) {
					OutputStream outputStream = new FileOutputStream(
							"././static/images/" + filePart.getSubmittedFileName());
					IOUtils.copy(inputStream, outputStream);

					outputStream.close();
				}

				Organizacija org = new Organizacija();
				org.setIme(namePart);
				org.setOpis(descPart);
				org.setLogo("images/" + filePart.getSubmittedFileName());
				dbm.getOrganizacije().add(org);
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		get("/main/org/getDiskovi/:id", (req, res) -> {

			String org_id = req.params("id");
			for (Organizacija org : dbm.getOrganizacije()) {
				if (org.getIme().equalsIgnoreCase(org_id)) {
					return g.toJson(org.getSlobodniDiskovi());
				}
			}
			return g.toJson(new ArrayList<Disk>());

		});
		
		get("/main/org/getEditDiskovi/:id", (req, res) -> {
			String vm_id = req.params("id");
			VirtuelnaMasina vm_curr = null;
			
			for (VirtuelnaMasina vm : dbm.getvMasine()) {
				if(vm.getIme().equalsIgnoreCase(vm_id)) {
					vm_curr = vm;
					break;
				}
			}
			
			Organizacija org_curr = null;
			for(Organizacija org : dbm.getOrganizacije()) {
				if(org.getIme().equalsIgnoreCase(vm_curr.getOrganizacija())) {
					org_curr = org;
					break;
				}
			}
			
			ArrayList<Disk> diskovi = org_curr.getSlobodniDiskovi();
			diskovi.addAll(vm_curr.getDiskovi());
			
			return g.toJson(diskovi);
		});

		get("/main/org/getOrganizacija/:org_id", (req, res) -> {

			String id = req.params("org_id");

			for (Organizacija o : dbm.getOrganizacije()) {
				if (o.getIme().equalsIgnoreCase(id))
					return g.toJson(o);
			}

			return null;
		});

		post("/main/org/editOrganization/:org_id", (req, res) -> {
			if (getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
				String id = req.params("org_id");

				Part filePart = req.raw().getPart("i_file");
				String namePart = req.queryParams("name");
				String descPart = req.queryParams("desc");

				if (!id.equalsIgnoreCase(namePart)) {
					for (Organizacija o : dbm.getOrganizacije()) {
						if (namePart.equalsIgnoreCase(o.getIme())) {
							return "FAILED";
						}
					}
				}

				String oldPath = null;
				Organizacija edited = null;
				for (Organizacija org : dbm.getOrganizacije()) {
					if (org.getIme().equalsIgnoreCase(id)) {
						edited = org;
						break;
					}
				}

				edited.setIme(namePart);

				for (VirtuelnaMasina vm : edited.getVirtuelneMasine()) {
					vm.setOrganizacija(edited.getIme());
					for (Disk d : vm.getDiskovi()) {
						d.setOrganizacija(edited.getIme());
					}
				}

				for (Disk d : edited.getDiskovi()) {
					d.setOrganizacija(edited.getIme());
				}

				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if (vm.getOrganizacija().equalsIgnoreCase(id)) {
						vm.setOrganizacija(edited.getIme());
						for (Disk d : vm.getDiskovi()) {
							d.setOrganizacija(edited.getIme());

						}
					}
				}

				for (Disk d : dbm.getDiskovi()) {
					if (d.getOrganizacija().equalsIgnoreCase(id))
						d.setOrganizacija(edited.getIme());
				}

				for (Korisnik k : users.getSviKorisnici().values()) {
					if (!k.getUloga().equals(Uloga.SUPER_ADMIN)) {
						if (k.getOrganizacija().equalsIgnoreCase(id))
							k.setOrganizacija(edited.getIme());
					}
				}

				edited.setOpis(descPart);
				oldPath = edited.getLogo();

				try (InputStream inputStream = filePart.getInputStream()) {
					OutputStream outputStream = new FileOutputStream(
							"././static/images/" + filePart.getSubmittedFileName());
					IOUtils.copy(inputStream, outputStream);

					outputStream.close();
				}

				edited.setLogo("images/" + filePart.getSubmittedFileName());
				File oldImage = new File("../../static/" + oldPath);
				oldImage.delete();

				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		get("/main/org/getDrives", (req, res) -> {

			if (getKorisnik(req).getUloga() == Uloga.SUPER_ADMIN) {
				return g.toJson(dbm.getDiskovi());
			} else {
				ArrayList<Disk> temp = new ArrayList<Disk>();
				for (Disk d : dbm.getDiskovi()) {
					if (d.getOrganizacija().equalsIgnoreCase(getKorisnik(req).getOrganizacija())) {
						temp.add(d);
					}
				}
				return g.toJson(temp);

			}
			

		});
		
		get("/main/org/getDrive/:drive_id", (req, res) -> {
			
			String id = req.params("drive_id");
			
			for (Disk d : dbm.getDiskovi()) {
				if(id.equalsIgnoreCase(d.getIme()))
					return g.toJson(d);
			}
			
			return "FAILED";
			
		});
		
		
		get("/main/org/getVMDrive/:drive_id", (req, res) -> {
			
			String id = req.params("drive_id");
			
			for (Disk d : dbm.getDiskovi()) {
				if(d.getIme().equalsIgnoreCase(id)) {
					for (Organizacija o : dbm.getOrganizacije()) {
						if(o.getIme().equalsIgnoreCase(d.getOrganizacija())) {
							return g.toJson(o.getVirtuelneMasine());
						}
					}
				}
			}
			
			return "FAILED";
			
			
		});

		post("/main/org/addDrive", (req, res) -> {
			if(getKorisnik(req).getUloga() != Uloga.KORISNIK) {
			String payload = req.body();

			DriveInfo info = g.fromJson(payload, DriveInfo.class);
			
			for (Disk d : dbm.getDiskovi()) {
				if(d.getIme().equalsIgnoreCase(info.ime))
					return "FAILED";
			}
			
			if(info.virtuelnaMasina == null || info.virtuelnaMasina.equalsIgnoreCase("")  ) info.virtuelnaMasina = null;
			
			Disk d = new Disk(info.ime, info.organizacija, DiskTip.valueOf(info.tip.toUpperCase()), Integer.parseInt(info.kapacitet), info.virtuelnaMasina);
			
			
			
			for (Organizacija o : dbm.getOrganizacije()) {
				if(o.getIme().equalsIgnoreCase(d.getOrganizacija())) {
					o.getDiskovi().add(d);
					if(info.virtuelnaMasina != null) {
						for (VirtuelnaMasina vm : o.getVirtuelneMasine()) {
							if(vm.getIme().equalsIgnoreCase(info.virtuelnaMasina)){
								vm.dodajDisk(d);
								break;
							}
						}
					}
					break;
				}
				
			}
			
			if (info.virtuelnaMasina != null) {
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if(vm.getIme().equalsIgnoreCase(info.virtuelnaMasina)) {
						vm.dodajDisk(d);
						break;
					}
				}
			}
			
			dbm.getDiskovi().add(d);
			
			dbm.SacuvajPromene();
			return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});

		post("/main/org/editDrive/:drive_id", (req, res) -> {
			if(getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String payload = req.body();
				DriveInfo info = g.fromJson(payload, DriveInfo.class);
				String id = req.params("drive_id");
				
				if(info.virtuelnaMasina == null || info.virtuelnaMasina.equalsIgnoreCase("")  ) info.virtuelnaMasina = null;
				System.out.println(id);
				System.out.println(info.ime);
				if(!id.equalsIgnoreCase(info.ime)) {
					for (Disk d : dbm.getDiskovi()) {
						if(d.getIme().equalsIgnoreCase(info.ime))
							return "FAILED";
					}
				}
				
				Disk edited = null;
				
				for (Disk d : dbm.getDiskovi()) {
					if(d.getIme().equalsIgnoreCase(id)) {
						edited = d;
					}
				}
				
				String vm_old = null;
				
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if(vm.getIme().equalsIgnoreCase(edited.getVirtuelnaMasina())) {
						vm.getDiskovi().remove(edited);
					}
				}
				
				Organizacija org = null;
				
				for (Organizacija o : dbm.getOrganizacije()) {
					if(o.getIme().equalsIgnoreCase(edited.getOrganizacija())) {
						org = o;
						break;
					}
				}
				
				for (VirtuelnaMasina vm : org.getVirtuelneMasine()) {
					vm.getDiskovi().remove(edited);
				}
				
				org.getDiskovi().remove(edited);
				
				edited.setIme(info.ime);
				edited.setKapacitet(Integer.parseInt(info.kapacitet));
				edited.setTip(DiskTip.valueOf(info.tip.toUpperCase()));
				edited.setVirtuelnaMasina(info.virtuelnaMasina);				
				
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if(vm.getIme().equalsIgnoreCase(edited.getVirtuelnaMasina())) {
						vm.dodajDisk(edited);
						break;
					}
				}
				
				for (VirtuelnaMasina vm : org.getVirtuelneMasine()) {
					if(vm.getIme().equalsIgnoreCase(edited.getVirtuelnaMasina())) {
						vm.dodajDisk(edited);
						break;
					}
				}
				
				org.getDiskovi().add(edited);
				
				dbm.SacuvajPromene();
				return "SUCCESS";
				
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
		});
		
		post("/main/org/deleteDrive/:drive_id", (req, res) -> {
			if(getKorisnik(req).getUloga() != Uloga.KORISNIK) {
				String id = req.params("drive_id");
				
				Disk delete = null;
				for (Disk d : dbm.getDiskovi()) {
					if (d.getIme().equalsIgnoreCase(id)) {
						delete = d;
						break;
					}
					
				}
				
				
				
				dbm.getDiskovi().remove(delete);
				
				for (VirtuelnaMasina vm : dbm.getvMasine()) {
					if(vm.getIme().equalsIgnoreCase(delete.getVirtuelnaMasina())) {
						vm.obrisiDisk(delete);
						break;
					}
				}
				
				for (Organizacija o : dbm.getOrganizacije()) {
					if(o.getIme().equalsIgnoreCase(delete.getOrganizacija())) {
						o.getDiskovi().remove(delete);
						for (VirtuelnaMasina vm : o.getVirtuelneMasine()) {
							if(vm.getIme().equalsIgnoreCase(delete.getVirtuelnaMasina())) {
								vm.obrisiDisk(delete);
							}
						}
					}
				}
				
				dbm.SacuvajPromene();
				return "SUCCESS";
			} else {
				res.status(403);
				return "UNAUTHORIZED";
			}
			
			
		});
		
		
		get("/main/org/getReceipt", (req, res) -> {
			
			String payload = req.body();
			System.out.println(payload);
			ActInfo info = g.fromJson(payload, ActInfo.class);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			Date dateP = formatter.parse(req.queryParams("datumPaljenja"));
			Date dateG = formatter.parse(req.queryParams("datumGasenja"));
			
			Receipt rec = new Receipt();
			System.out.println(dateP);
			System.out.println(dateG);
			for (Organizacija o : dbm.getOrganizacije()) {
				if(o.getIme().equalsIgnoreCase(getKorisnik(req).getOrganizacija())) {
					for (VirtuelnaMasina vm : o.getVirtuelneMasine()) {
						double cena = vm.izracunajCenu(dateP, dateG);
						System.out.println(vm.getIme() + ", " + cena);
						if(cena != 0) {
							rec.getVms().add(new VMPrice(vm.getIme(),Math.round(cena)));
							rec.setTotalCost(rec.getTotalCost() + Math.round(cena));
						}
					}
					for (Disk d : o.getDiskovi()) {
						double cena = d.izracunajCenu(dateP, dateG);
						rec.getDrives().add(new DrivePrice(d.getIme(), Math.round(cena)));
						rec.setTotalCost(rec.getTotalCost() + Math.round(cena));
					}
				}
				
			}
			
			System.out.println(rec.getTotalCost());
			return g.toJson(rec);
			
			
		});
		
	}

	// DELETE KATEGORIJA => SUCCESS, FAIL, ATTACHED(ako postoji vm koja je te
	// kategorije)

	private static Korisnik getKorisnik(Request req) {
		Session ss = req.session(true);
		Korisnik user = ss.attribute("korisnik");
		if (user == null) {
			String payload = req.body();
			LoginInfo login = g.fromJson(payload, LoginInfo.class);

			user = users.nadjiKorisnika(login.email);
			ss.attribute("korisnik", user);
		}

		return user;
	}
}
