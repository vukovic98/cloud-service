package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class VirtuelnaMasina {
	private String ime;
	private String organizacija;
	private Kategorija kategorija;
	private int brojJezgara;
	private int RAM;
	private int brojGPUJezgara;
	private ArrayList<Disk> diskovi;
	private ArrayList<Aktivnost> aktivnosti;

	public VirtuelnaMasina() {
		super();
		this.diskovi = new ArrayList<Disk>();
		this.aktivnosti = new ArrayList<Aktivnost>();
	}

	public int getBrojJezgara() {
		return brojJezgara;
	}

	public void setBrojJezgara(int brojJezgara) {
		this.brojJezgara = brojJezgara;
	}

	public int getRAM() {
		return RAM;
	}

	public void setRAM(int rAM) {
		RAM = rAM;
	}

	public int getBrojGPUJezgara() {
		return brojGPUJezgara;
	}

	public void setBrojGPUJezgara(int brojGPUJezgara) {
		this.brojGPUJezgara = brojGPUJezgara;
	}

	public VirtuelnaMasina(String ime, String org, Kategorija kategorija, ArrayList<Disk> diskovi,
			ArrayList<Aktivnost> aktivnosti) {
		super();
		this.ime = ime;
		this.organizacija = org;
		this.kategorija = kategorija;
		this.brojJezgara = kategorija.getBrojJezgara();
		this.brojGPUJezgara = kategorija.getBrojGPUJezgara();
		this.RAM = kategorija.getRAM();
		this.diskovi = diskovi;
		this.aktivnosti = aktivnosti;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
		this.brojGPUJezgara = kategorija.getBrojGPUJezgara();
		this.brojJezgara = kategorija.getBrojJezgara();
		this.RAM = kategorija.getRAM();
	}

	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}

	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}

	public ArrayList<Aktivnost> getAktivnosti() {
		return aktivnosti;
	}

	public void setAktivnosti(ArrayList<Aktivnost> aktivnosti) {
		this.aktivnosti = aktivnosti;
	}

	@Override
	public String toString() {
		return "VirtuelnaMasina [ime=" + ime + ", kategorija=" + kategorija + ", diskovi=" + diskovi + ", aktivnosti="
				+ aktivnosti + "]";
	}

	public String getOrganizacija() {
		return organizacija;
	}

	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else {
			VirtuelnaMasina vm = (VirtuelnaMasina) obj;
			return this.ime.equalsIgnoreCase(vm.getIme());
		}
	}

	public Disk nadjiDisk(String ime) {
		for (Disk d : diskovi) {
			if (d.getIme().equalsIgnoreCase(ime)) {
				return d;
			}
		}
		return null;
	}

	public void dodajDisk(Disk d) {
		diskovi.add(d);
	}

	public void obrisiDisk(Disk d) {
		diskovi.remove(d);
	}

	public void dodajAktivnost(Aktivnost a) {
		aktivnosti.add(a);
	}

	public void obrisiAktivnost(Aktivnost a) {
		aktivnosti.remove(a);
	}

	public double izracunajCenu(Date start, Date end) {
		double cena = 0.0;
		double jezgroCenaSat = (25.0 / 30.0) / 24.0;
		double GPUJezgroCenaSat = (1.0 / 30.0) / 24.0;
		double RAMCenaSat = (15.0 / 30.0) / 24.0;
		for (Aktivnost aktivnost : aktivnosti) {
			System.out.println("[" + aktivnost.getDatumPaljenja() + ", " + aktivnost.getDatumGasenja() + "]");
			if (aktivnost.getDatumPaljenja().after(start) && aktivnost.getDatumGasenja().before(end)) {
				long diff = aktivnost.getDatumGasenja().getTime() - aktivnost.getDatumPaljenja().getTime();
				// long sati = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
				long sati = diff / (60 * 60 * 1000);
				System.out.println(sati);
				cena = cena + (sati * jezgroCenaSat * brojGPUJezgara + sati * GPUJezgroCenaSat * brojGPUJezgara
						+ sati * RAMCenaSat * RAM);
				System.out.println(cena);
			} else {
				if (start.after(aktivnost.getDatumPaljenja()) && start.before(aktivnost.getDatumGasenja())) {
					if (end.after(aktivnost.getDatumGasenja())) {
						long diff = aktivnost.getDatumGasenja().getTime() - start.getTime();
						long sati = diff / (60 * 60 * 1000);
						cena = cena + (sati * jezgroCenaSat * brojGPUJezgara + sati * GPUJezgroCenaSat * brojGPUJezgara
								+ sati * RAMCenaSat * RAM);
						System.out.println(cena);
					} else {
						long diff = end.getTime() - start.getTime();
						long sati = diff / (60 * 60 * 1000);
						cena = cena + (sati * jezgroCenaSat * brojGPUJezgara + sati * GPUJezgroCenaSat * brojGPUJezgara
								+ sati * RAMCenaSat * RAM);
						System.out.println(cena);
					}
				} else if (end.after(aktivnost.getDatumPaljenja()) && end.before(aktivnost.getDatumGasenja())) {
					if (start.before(aktivnost.getDatumPaljenja())) {
						long diff = end.getTime() - aktivnost.getDatumPaljenja().getTime();
						long sati = diff / (60 * 60 * 1000);
						System.out.println(sati);
						cena = cena + (sati * jezgroCenaSat * brojGPUJezgara + sati * GPUJezgroCenaSat * brojGPUJezgara
								+ sati * RAMCenaSat * RAM);
						System.out.println(cena);
					} else {
						long diff = end.getTime() - start.getTime();
						long sati = diff / (60 * 60 * 1000);
						cena = cena + (sati * jezgroCenaSat * brojGPUJezgara + sati * GPUJezgroCenaSat * brojGPUJezgara
								+ sati * RAMCenaSat * RAM);
						System.out.println(cena);
					}
				} else
					continue;
			}
		}

		return cena;
	}

}
