package model;

public class Kategorija {
	private String ime;
	private int brojJezgara;
	private int RAM;
	private int brojGPUJezgara;

	public Kategorija() {
		super();
	}

	public Kategorija(String ime, int brojJezgara, int rAM, int brojGPUJezgara) {
		super();
		this.ime = ime;
		this.brojJezgara = brojJezgara;
		RAM = rAM;
		this.brojGPUJezgara = brojGPUJezgara;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public int getBrojJezgara() {
		return brojJezgara;
	}

	public void setBrojJezgara(int brojJezgara) {
		if (brojJezgara > 0) {
			this.brojJezgara = brojJezgara;
		}
	}

	public int getRAM() {
		return RAM;
	}

	@Override
	public String toString() {
		return "Kategorija [ime=" + ime + ", brojJezgara=" + brojJezgara + ", RAM=" + RAM + ", brojGPUJezgara="
				+ brojGPUJezgara + "]";
	}

	public void setRAM(int RAM) {
		if (RAM > 0) {
			this.RAM = RAM;
		}
	}

	public int getBrojGPUJezgara() {
		return brojGPUJezgara;
	}

	public void setBrojGPUJezgara(int brojGPUJezgara) {
		this.brojGPUJezgara = brojGPUJezgara;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else {
			Kategorija k = (Kategorija) obj;
			return this.ime.equals(k.getIme());
		}
	}

}
