package model;

public class Korisnik {
	private String email;
	private String lozinka;
	private String ime;
	private String prezime;
	private String organizacija;
	private Uloga uloga;

	public Korisnik() {
		super();
	}

	public Korisnik(String email, String lozinka, String ime, String prezime, String organizacija, Uloga uloga) {
		super();
		this.email = email;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.organizacija = organizacija;
		this.uloga = uloga;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getOrganizacija() {
		return organizacija;
	}

	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	@Override
	public String toString() {
		return this.ime + ", " + this.prezime + ", " + this.email + ", " + this.organizacija;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		if(o instanceof Korisnik) {
			Korisnik k = (Korisnik) o;
			if(k.getEmail().equalsIgnoreCase(this.email))
				return true;
			else
				return false;
		} else {
			return false;
		}
	}
}
