package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class DatabaseManager {
	private ArrayList<Disk> diskovi = new ArrayList<Disk>();
	private ArrayList<Aktivnost> aktivnosti = new ArrayList<Aktivnost>();
	private ArrayList<VirtuelnaMasina> vMasine = new ArrayList<VirtuelnaMasina>();
	private ArrayList<Organizacija> organizacije = new ArrayList<Organizacija>();
	private ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>();
	public DatabaseManager() {
		UcitajDiskove();
		UcitajKategorije();
		UcitajVMasine();
		UcitajOrganizacije();
	}
	
	public void SacuvajPromene()
	{
		SacuvajDiskove();
		SacuvajKategorije();
		SacuvajOrganizacije();
		SacuvajVMasine();
		
	}
	
	private void UcitajDiskove() {
		String PATH = "./files/diskovi.json";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(PATH));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			Type arrayType = new TypeToken<ArrayList<Disk>>(){}.getType();
			this.diskovi = gson.fromJson(br, arrayType);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void SacuvajDiskove() {
		String PATH = "./files/diskovi.json";
		try {
			Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			FileWriter fwr = new FileWriter(PATH);
			g.toJson(this.diskovi, fwr);
			fwr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void UcitajKategorije() {
		String PATH = "./files/kategorije.json";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(PATH));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			Type arrayType = new TypeToken<ArrayList<Kategorija>>(){}.getType();
			this.kategorije = gson.fromJson(br, arrayType);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void SacuvajKategorije() {
		String PATH = "./files/kategorije.json";
		try {
			Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			FileWriter fwr = new FileWriter(PATH);
			g.toJson(this.kategorije, fwr);
			fwr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void UcitajVMasine() {
		String PATH = "./files/virtuelneMasine.json";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(PATH));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			Type arrayType = new TypeToken<ArrayList<VirtuelnaMasina>>(){}.getType();
			this.vMasine = gson.fromJson(br, arrayType);
			for (VirtuelnaMasina vm : this.vMasine) {
				if(vm.getDiskovi() == null) vm.setDiskovi(new ArrayList<Disk>());
				if(vm.getAktivnosti() == null) vm.setAktivnosti(new ArrayList<Aktivnost>());
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void SacuvajVMasine() {
		String PATH = "./files/virtuelneMasine.json";
		try {
			Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			FileWriter fwr = new FileWriter(PATH);
			g.toJson(this.vMasine, fwr);
			fwr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void UcitajOrganizacije() {
		String PATH = "./files/organizacije.json";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(PATH));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			Type arrayType = new TypeToken<ArrayList<Organizacija>>(){}.getType();
			this.organizacije = gson.fromJson(br, arrayType);
			for (Organizacija o : this.organizacije) {
				if(o.getKorisnici() == null) o.setKorisnici(new ArrayList<Korisnik>());
				if(o.getVirtuelneMasine() == null) o.setVirtuelneMasine(new ArrayList<VirtuelnaMasina>());
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void SacuvajOrganizacije() {
		String PATH = "./files/organizacije.json";
		try {
			Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").setPrettyPrinting().create();
			FileWriter fwr = new FileWriter(PATH);
			g.toJson(this.organizacije, fwr);
			fwr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Organizacija OrganizacijaZaVM(VirtuelnaMasina vm) {
		for (Organizacija o : organizacije) {
			for (VirtuelnaMasina virt : o.getVirtuelneMasine()) {
				if(virt.equals(vm)) return o;
			}
		}
		
		return null;
	}
	
	public Organizacija OrganizacijaZaKorisnika(Korisnik k) {
		for (Organizacija o : organizacije) {
			for(Korisnik kor : o.getKorisnici()) {
				if(kor.equals(k)) return o;
			}
		}
		
		return null;
	}

	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}

	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}

	public ArrayList<Aktivnost> getAktivnosti() {
		return aktivnosti;
	}

	public void setAktivnosti(ArrayList<Aktivnost> aktivnosti) {
		this.aktivnosti = aktivnosti;
	}

	public ArrayList<VirtuelnaMasina> getvMasine() {
		return vMasine;
	}

	public void setvMasine(ArrayList<VirtuelnaMasina> vMasine) {
		this.vMasine = vMasine;
	}

	public ArrayList<Organizacija> getOrganizacije() {
		return organizacije;
	}

	public void setOrganizacije(ArrayList<Organizacija> organizacije) {
		this.organizacije = organizacije;
	}

	public ArrayList<Kategorija> getKategorije() {
		return kategorije;
	}

	public void setKategorije(ArrayList<Kategorija> kategorije) {
		this.kategorije = kategorije;
	}
	
	
}
