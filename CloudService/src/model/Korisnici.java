package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

public class Korisnici {
	private HashMap<String, Korisnik> korisnici;

	public Korisnici() {
		super();
		this.korisnici = new HashMap<String, Korisnik>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("./files/korisnici.json"));
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			Type hashType = new TypeToken<HashMap<String,Korisnik>>(){}.getType();
			this.korisnici = gson.fromJson(br, hashType);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Korisnici(HashMap<String, Korisnik> sviKorisnici) {
		super();
		this.korisnici = sviKorisnici;
	}

	public HashMap<String, Korisnik> getSviKorisnici() {
		return korisnici;
	}

	public void setSviKorisnici(HashMap<String, Korisnik> sviKorisnici) {
		this.korisnici = sviKorisnici;
	}

	public void dodajKorisnika(Korisnik k) {
		this.korisnici.put(k.getEmail(), k);
	}

	public void obrisiKorisnika(String email) {
		this.korisnici.remove(email);
	}

	public Korisnik nadjiKorisnika(String email) {
		return this.korisnici.get(email);
	}
	
	public void writeJson() {
		Gson gson = new Gson();
		try {
			FileWriter fw = new FileWriter(new File("./files/korisnici.json"));
			gson.toJson(this.korisnici,fw);
			fw.close();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
