package model;

import java.util.ArrayList;

public class Organizacija {
	private String ime;
	private String opis;
	private String logo;
	private ArrayList<Korisnik> korisnici;
	private ArrayList<VirtuelnaMasina> virtuelneMasine;
	private ArrayList<Disk> diskovi;
	public Organizacija() {
		super();
		this.korisnici = new ArrayList<Korisnik>();
		this.virtuelneMasine = new ArrayList<VirtuelnaMasina>();
		this.diskovi = new ArrayList<Disk>();
	}

	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}

	public ArrayList<Disk> getSlobodniDiskovi(){
		ArrayList<Disk> temp = new ArrayList<Disk>();
		for (Disk disk : this.diskovi) {
			if(disk.getVirtuelnaMasina() == null || disk.getVirtuelnaMasina() == "") {
				temp.add(disk);
			}
		}
		return temp;
	}
	
	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}

	public Organizacija(String ime, String opis, String logo, ArrayList<Korisnik> korisnici,
			ArrayList<VirtuelnaMasina> virtuelneMasine, ArrayList<Disk> diskovi) {
		super();
		this.ime = ime;
		this.opis = opis;
		this.logo = logo;
		this.korisnici = korisnici;
		this.virtuelneMasine = virtuelneMasine;
		this.diskovi = diskovi;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public ArrayList<Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(ArrayList<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}

	public ArrayList<VirtuelnaMasina> getVirtuelneMasine() {
		return virtuelneMasine;
	}

	public void setVirtuelneMasine(ArrayList<VirtuelnaMasina> virtuelneMasine) {
		this.virtuelneMasine = virtuelneMasine;
	}

	public Korisnik nadjiKorisnika(String email) {
		for (Korisnik k : this.korisnici) {
			if (k.getEmail().equalsIgnoreCase(email))
				return k;
		}
		return null;
	}

	public void dodajKorisnika(Korisnik k) {
		this.korisnici.add(k);
	}

	public void obrisiKorisnika(String email) {
		for (Korisnik k : this.korisnici) {
			if (k.getEmail().equalsIgnoreCase(email)) {
				this.korisnici.remove(k);
				break;
			}
		}
	}

	public VirtuelnaMasina nadjiVirtuelnuMasinu(String ime) {
		for (VirtuelnaMasina vm : this.virtuelneMasine) {
			if (vm.getIme().equals(this.ime))
				return vm;
		}
		return null;
	}

	public void dodajVirtuelnuMasinu(VirtuelnaMasina vm) {
		this.virtuelneMasine.add(vm);
	}

	public void obrisiVirtuelnuMasinu(String ime) {
		for (VirtuelnaMasina vm : this.virtuelneMasine) {
			if (vm.getIme().equals(this.ime))
				this.virtuelneMasine.remove(vm);
		}
	}

	@Override
	public String toString() {
		return this.ime + ", " + this.opis;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o instanceof Organizacija) {
			Organizacija org = (Organizacija) o;
			if (org.getIme().equalsIgnoreCase(this.ime))
				return true;
			else
				return false;
		} else {
			return false;
		}
	}

}
