package model;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Disk {
	private String ime;
	private String organizacija;
	private DiskTip tip;
	private int kapacitet;
	private String virtuelnaMasina;

	public Disk() {
		super();
	}

	public Disk(String ime,String organizacija, DiskTip tip, int kapacitet, String virtuelnaMasina) {
		super();
		this.ime = ime;
		this.organizacija = organizacija;
		this.tip = tip;
		this.kapacitet = kapacitet;
		this.virtuelnaMasina = virtuelnaMasina;
	}
	
	public String getOrganizacija() {
		return organizacija;
	}

	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public DiskTip getTip() {
		return tip;
	}

	public void setTip(DiskTip tip) {
		this.tip = tip;
	}

	public int getKapacitet() {
		return kapacitet;
	}

	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}

	public String getVirtuelnaMasina() {
		return virtuelnaMasina;
	}

	public void setVirtuelnaMasina(String virtuelnaMasina) {
		this.virtuelnaMasina = virtuelnaMasina;
	}

	public double izracunajCenu(Date start, Date end) {
		double cena = 0.0;
		double cenaSat = tip == DiskTip.HDD?(0.1/30)/24:(0.3/30)/24;

		long diff = end.getTime() - start.getTime();
		long sati = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
		cena = cena + sati*cenaSat*kapacitet;
		
		return cena;
	}
	
	@Override
	public String toString() {
		return "Disk [ime=" + ime + ", tip=" + tip + ", kapacitet=" + kapacitet + ", virtuelnaMasina=" + virtuelnaMasina
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else {
			Disk d = (Disk) obj;
			return this.ime.equals(d.ime);
		}
	}
}
