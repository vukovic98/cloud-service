package model;

import java.util.Date;

public class Aktivnost {
	private Date datumPaljenja;
	private Date datumGasenja;

	public Aktivnost() {
		super();
	}

	public Aktivnost(Date datumPaljenja, Date datumGasenja) {
		super();
		this.datumPaljenja = datumPaljenja;
		this.datumGasenja = datumGasenja;
	}

	public Date getDatumPaljenja() {
		return datumPaljenja;
	}

	public void setDatumPaljenja(Date datumPaljenja) {
		this.datumPaljenja = datumPaljenja;
	}

	public Date getDatumGasenja() {
		return datumGasenja;
	}

	public void setDatumGasenja(Date datumGasenja) {
		this.datumGasenja = datumGasenja;
	}

	@Override
	public String toString() {
		return "Aktivnost [datumPaljenja=" + datumPaljenja + ", datumGasenja=" + datumGasenja + "]";
	}

}
