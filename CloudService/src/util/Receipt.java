package util;

import java.util.ArrayList;

public class Receipt {
	private ArrayList<VMPrice> vms;
	private ArrayList<DrivePrice> drives;
	private double totalCost;

	public ArrayList<VMPrice> getVms() {
		return this.vms;
	}

	public void setVms(ArrayList<VMPrice> vms) {
		this.vms = vms;
	}

	public ArrayList<DrivePrice> getDrives() {
		return this.drives;
	}

	public void setDrives(ArrayList<DrivePrice> drives) {
		this.drives = drives;
	}

	public double getTotalCost() {
		return this.totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public Receipt() {
		this.vms = new ArrayList<VMPrice>();
		this.drives = new ArrayList<DrivePrice>();
		this.totalCost = 0.0;
	}
}
