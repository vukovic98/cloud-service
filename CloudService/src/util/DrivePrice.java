package util;

public class DrivePrice{
	private String driveName;
	private double price;

	public DrivePrice(String driveName, double price) {
		super();
		this.driveName = driveName;
		this.price = price;
	}
	public String getVmName() { return this.driveName;}
	public void setVmName(String driveName) {this.driveName = driveName;}
	
	public double getPrice() {return this.price;}
	public void setPrice(double price) {this.price = price;}
}
