package util;

public class VMPrice{
	private String vmName;
	private double price;

	public VMPrice(String vmName, double price) {
		super();
		this.vmName = vmName;
		this.price = price;
	}
	public String getVmName() { return this.vmName;}
	public void setVmName(String vmName) {this.vmName = vmName;}
	
	public double getPrice() {return this.price;}
	public void setPrice(double price) {this.price = price;}
}
