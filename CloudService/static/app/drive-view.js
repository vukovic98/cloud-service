Vue.component ("drive-view", {
	data: function () {
		return {
			drive: null,
			korisnik: null,
			success: true,
			name: null,
			cap: null,
			type: null,
			vm: null,
			virtuelneMasine: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>

		<div class="card text-center col-md-8" style="float: left;">
			<div class="card-header">
				<big>Drive info</big>
			</div>
			<div class="card-body">
				<div class="mb-3">
					<img src="./images/drive.png" class="user-image">
				</div>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Name
						<h4>
							
							<span class="badge badge-light">{{drive.ime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Organization
						<h4>
							<span class="badge badge-light">{{drive.organizacija}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Type
						<h4>
							<span class="badge badge-light">{{drive.tip}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Capacity
						<h4>
							<span class="badge badge-light">{{drive.kapacitet}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Virtual machine
						<h4>
							<span class="badge badge-light">{{drive.virtuelnaMasina}}</span>
						</h4>
					</li>
				</ul>
			</div>
		</div>

		<div class="card" style="height: 100%">
			<div v-if="korisnik.uloga != 'KORISNIK'" class="card-header">
				<big>Edit drive</big>
			</div>
			<div v-if="korisnik.uloga != 'KORISNIK'" class="card-body col-md-12" style="float: right; height: 100%">
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" name="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>Type</label>
						<select class="custom-select" id="type" name="type" v-model="type">
							<option disabled selected hidden>Choose</option>
							<option>HDD</option>
							<option>SSD</option>
						 </select>
					</div>
					<div class="form-group col-md-12">
						<label>Capacity</label> <input type="number"
							class="form-control" id="cap" v-model="cap" name="cap" placeholder="Capacity">
					</div>
					<div class="form-group col-md-12">
						<label>Virtual machine</label>
						<select class="custom-select" id="vm" name="vm" v-model="vm">
							<option disabled selected hidden>Choose</option>
							<template v-for="v in virtuelneMasine">
								<option>{{v.ime}}</option>
							</template>
						 </select>
					</div>
					<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>Drive already exists!
			  		</div>
			  		
					<div v-if="korisnik.uloga != 'KORISNIK'" class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="editDrive()">Submit</button>
					</div>
					<div v-if="korisnik.uloga == 'SUPER_ADMIN'" class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="deleteDrive()">Delete</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		editDrive: function() {
			if(this.korisnik.uloga != "KORISNIK") {
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.type == null || this.type == "") {
					document.getElementById("type").classList.add("is-invalid");
				} else {
					document.getElementById("type").classList.remove("is-invalid");
				}
				
				if(this.cap == null || this.cap == "") {
					document.getElementById("cap").classList.add("is-invalid");
				} else {
					document.getElementById("cap").classList.remove("is-invalid");
				}
				
				if(this.vm == null || this.vm == "") {
					document.getElementById("vm").classList.add("is-invalid");
				} else {
					document.getElementById("vm").classList.remove("is-invalid");
				}
				
				
			
				if(this.name != "" && this.name != null && this.type != null && this.type != "" && this.cap != null && 
						this.cap != "" && this.vm != null && this.vm != "") {
					axios
					.post('/main/org/editDrive/' + this.drive.ime, {
						"ime":this.name,
						"tip":this.type,
						"kapacitet":this.cap,
						"virtuelnaMasina":this.vm
					})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/drive-view/' + this.name);
							location.reload();
						} else{
							this.success = false;
						}
					});
				}
			}
		},
		deleteDrive: function() {
			if(this.korisnik.uloga == "SUPER_ADMIN") {
				axios
				.post('/main/org/deleteDrive/' + this.drive.ime)
				.then(response => {
					if(response.data === "SUCCESS") {
						this.$router.push('/drives');
					} else {
						console.log("ERROR");
					}
				});
			}
		}
	},
	
	mounted () {
        axios
	        .get('main/org/getDrive/' + this.$route.params.drive_id)
	        .then(response => {
	      	  this.drive = response.data;
	        });
	        axios
	        .get('main/org/getVMDrive/' + this.$route.params.drive_id)
	        .then(response => {
	      	  this.virtuelneMasine = response.data;
	        })
        .catch(error => {
        	console.log("403 - Forbidden for USER!");
        });
        axios
	        .get('main/session/getUser')
	        .then(response => {
	       	 this.korisnik = response.data;
        });
    },
});