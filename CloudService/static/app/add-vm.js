Vue.component ("add-vm", {
	    data: function() {
	      return {
	        show: false,
	        ime: null,
	        organizacije: null,
	        kategorije: null,
	        organizacija: null,
	        kategorija: null,
	        korisnik: '',
	        diskovi: null,
	        disk: [],
	        success: true,
	        show: false
	      }
	    },
	template: `
	<div>
		<button class="btn btn-primary btn-lg btn-block" v-on:click="open()">Add machine</button>
		    <div id="myModal" class="modal-my">
				
			  <!-- Modal content -->
			  <div class="modal-content-my">
			  	
			  
		<div class="col-md-12 order-md-2 mb-4 ">
			<ul class="list-group mb-3">
				<li class="list-group-item justify-content-between lh-condensed mt-4">
					<div>
						<h4 class="my-0 pb-3">New VM</h4>
						<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">Name</div>
									</div>
									<input type="text" class="form-control" name="name" id="name" v-model="ime">
								</div>
							</div>
					</div>

				</li>


				<li class="list-group-item justify-content-between lh-condensed">
						<div>
							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">Category</div>
									</div>
									<select class="custom-select" id="kat" name="kat" v-model="kategorija">
									    <template v-for="k in kategorije">
									    	<option>{{k.ime}}</option>
									    </template>
									  </select>
								</div>
							</div>
						</div>
				</li>


				<li class="list-group-item  justify-content-between lh-condensed">
						<div>
							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">Organization</div>
									</div>
									<select class="custom-select" id="org" name="org" v-on:change="inDrives" v-model="organizacija">
										<template v-for="o in organizacije">
									    	<option>{{o.ime}}</option>
									    </template>
									 </select>
								</div>
							</div>


						</div>
				</li>
				
				<li v-if="show" class="list-group-item  justify-content-between lh-condensed">
						<div>
							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">Drives</div>
									</div>
									<select class="custom-select" id="disk" name="disk" multiple v-model="disk" style="max-height: 105px;">
										<template v-for="d in diskovi">
									    	<option>{{d.ime}}</option>
									    </template>
									 </select>
								</div>
							</div>


						</div>
				</li>

				<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  		<strong>Error!</strong> Virtual machine with this name already exists.
				</div>

				<div style="margin-top: 3%">
					<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="check()">Add</button>
				</div>
				<div style="margin-top: 3%">
					<button type="button" class="btn btn-secondary btn-lg btn-block" v-on:click="close()">Exit</button>
				</div>
			</ul>
		</div>
			    
			    
			  </div>

			</div>
  </div>
	`,
	methods: {
		inDrives: function() {
			if(this.korisnik.uloga != "KORISNIK") {
				axios
				.get('/main/org/getDiskovi/' + this.organizacija)
				.then(response => {
					this.diskovi = response.data;
					this.show = true;
				});
			}
		},
		open: function() {
			if(this.korisnik.uloga != "KORISNIK")
				document.getElementById("myModal").style.display = "block";
		},
		check: function() {
			if(this.korisnik.uloga != "KORISNIK"){
				if (this.ime == "" || this.ime == null) {
					document.getElementById("name").placeholder = "Name must be inserted!"
					document.getElementById("name").classList.add("is-invalid");
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
	
				if (this.kategorija == "" || this.kategorija == null) {
					document.getElementById("kat").placeholder = "Category must be inserted!"
					document.getElementById("kat").classList.add("is-invalid");
				} else {
					document.getElementById("kat").classList.remove("is-invalid");
				}
				
				if (this.organizacija == "" || this.organizacija == null) {
					document.getElementById("org").classList.add("is-invalid");
				} else {
					document.getElementById("org").classList.remove("is-invalid");
				}
				
				if (this.disk.length == 0 || this.disk == null) {
					document.getElementById("disk").classList.add("is-invalid");
				} else {
					document.getElementById("disk").classList.remove("is-invalid");
				}
	
				if (this.ime != null && this.kategorija != null && this.organizacija != null && this.ime != "" && this.kategorija != "" && this.organizacija != ""
					&& this.disk.length != 0 && this.disk != null) {
					axios
					.post('/main/org/addVm', {
						"vm_ime":this.ime,
						"kat":this.kategorija,
						"org":this.organizacija,
						"drives":this.disk
					})
					.then(response => {
						if(response.data == "SUCCESS"){
							this.sucess = true;
							document.getElementById("myModal").style.display = "none";
							this.$router.push('/vmpage');
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		},
		close: function() {
			document.getElementById("myModal").style.display = "none";
		},
	},
	mounted () {
		axios
			.get('/main/org/getKategorije')
			.then(response => {
				this.kategorije = response.data;
			})
			.catch(error => {
				console.log("403 - Forbidden for USER.");
		});
		
		axios
			.get('/main/org/getOrganizacije')
			.then(response => {
				this.organizacije = response.data;
			})
			.catch(error => {
				console.log("403 - Forbidden for USER.");
		});
		axios
	        .get('main/session/getUser')
	        .then(response => {
	       	 this.korisnik = response.data;
        });
	},
});