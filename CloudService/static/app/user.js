Vue.component ("user", {
	data: function () {
		return {
			korisnik: '',
			validEMAIL: true,
			success: false,
			notSuccess: false,
			email: null,
			name: null,
			lastname: null,
			pass: null,
			pass2: null,
			passError: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="card text-center col-md-8" style="float: left;">
			<div class="card-header">
				<big>User info</big>
			</div>
			<div class="card-body" style="align-self: center;">
				<div class="mb-3">
					<img src="./images/user.png" class="rounded-circle user-image">
				</div>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Name
						<h4>
							
							<span class="badge badge-light">{{korisnik.ime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Lastname
						<h4>
							<span class="badge badge-light">{{korisnik.prezime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Email
						<h4>
							<span class="badge badge-light">{{korisnik.email}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Role
						<h4>
							<span class="badge badge-light">{{korisnik.uloga}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Organization
						<h4>
							<span class="badge badge-light">{{korisnik.organizacija}}</span>
						</h4>
					</li>
				</ul>
			</div>
		</div>







		<div class="card" style="height: 100%">
			<div class="card-header">
				<big>Edit info</big>
			</div>
			<div class="card-body col-md-12" style="float: right; height: 100%">

					<div class="form-group col-md-12">
						<label>Email</label> <input type="text"
							class="form-control" id="email" v-model="email" placeholder="email@example.com">
					</div>
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>Lastname</label> <input type="text"
							class="form-control" id="lastname" v-model="lastname" placeholder="Lastname">
					</div>
					<div class="form-group col-md-12">
						<label>Password</label> <input type="password"
							class="form-control" id="pass" v-model="pass" placeholder="Password">
					</div>
					<div class="form-group col-md-12">
						<label>Repeat password</label> <input type="password"
							class="form-control" id="pass2" v-model="pass2" placeholder="Password">
					</div>
					
			  		<div v-if="success" class="alert alert-success alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Success!</strong> User info changed successfully!
					</div>
					<div v-if="!validEMAIL" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong> Email form is invalid.
					</div>
					<div v-if="notSuccess" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>User with inserted email already exists!
			  		</div>
			  		<div v-if="passError" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>Check your password!
			  		</div>
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="editSelf()">Submit</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		validEmail: function(email) {
		      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		      return re.test(email);
		},
		editSelf: function() {
			if(this.email == null || this.email == "") {
				document.getElementById("email").classList.add("is-invalid");
				document.getElementById("email").placeholder = "Mandatory!";
			} else {
				document.getElementById("email").classList.remove("is-invalid");
			}
			
			if(!this.validEmail(this.email)) {
				this.validEMAIL = false;
				document.getElementById("email").classList.add("is-invalid");
			} else {
				this.validEMAIL = true;
				document.getElementById("email").classList.remove("is-invalid");
			}
			
			if(this.name == null || this.name == "") {
				document.getElementById("name").classList.add("is-invalid");
				document.getElementById("name").placeholder = "Mandatory!";
			} else {
				document.getElementById("name").classList.remove("is-invalid");
			}
			
			if(this.lastname == null || this.lastname == "") {
				document.getElementById("lastname").classList.add("is-invalid");
				document.getElementById("lastname").placeholder = "Mandatory!";
			} else {
				document.getElementById("lastname").classList.remove("is-invalid");
			}
			
			if(this.pass == null || this.pass == "") {
				document.getElementById("pass").classList.add("is-invalid");
				document.getElementById("pass").placeholder = "Mandatory!";
			} else {
				document.getElementById("pass").classList.remove("is-invalid");
			}
			
			if(this.pass2 == null || this.pass2 == "") {
				document.getElementById("pass2").classList.add("is-invalid");
				document.getElementById("pass2").placeholder = "Mandatory!";
			} else {
				document.getElementById("pass2").classList.remove("is-invalid");
			}
			
			if(this.pass != this.pass2) {
				this.passError = true;
				
				document.getElementById("pass2").classList.add("is-invalid");
			} else {
				if((this.pass == "" || this.pass == null) && (this.pass2 == null || this.pass2 == "")){
					console.log("Both empty!");
					this.passError = false;
				} else {
					this.passError = false;
					document.getElementById("pass2").classList.remove("is-invalid");
				}
			}
		
			if(this.name != "" && this.name != null && this.email != null && this.email != "" && this.lastname != null && this.lastname != ""
				&& this.pass != null && this.pass != "" && this.pass2 != null && this.pass2 != "" && this.pass == this.pass2 && this.validEMAIL) {
				axios
				.post('/main/org/editSelf',  {
					"email":this.email,
					"name":this.name,
					"lastname":this.lastname,
					"password":this.pass
					})
				.then(response => {
					if(response.data === "SUCCESS") {
						this.success = true;
						this.notSuccess = false;
						this.$router.push('/user');
						location.reload();
					} else {
						this.success = false;
						this.notSuccess = true;
					}
				});
			}
		}
	},
	
	mounted () {
        axios
        .get('/main/session/getUser')
        .then(response => {
      	  this.korisnik = response.data;
        });
        
    },
});