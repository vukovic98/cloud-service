Vue.component ("receipt", {
	    data: function() {
	      return {
	        show: false,
	        korisnik: '',
	        success: true,
	        exists: true,
	        show: false,
	        dateFrom: null,
	        dateTo: null,
	        components: null
	      }
	    },
	template: `
	<div>
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="col-md-8 vm-table">
			<div class="input-group">
			  <div class="input-group-prepend">
			    <span class="input-group-text">Date from/Date to</span>
			  </div>
			  <input type="datetime-local" id="dateFrom" v-model="dateFrom" class="form-control">
			  <input type="datetime-local" id="dateTo" v-model="dateTo" class="form-control">
			  <button type="button" class="btn btn-primary ml-2" v-on:click="getReceipt()">Get receipt</button>
			</div>
		<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2" style="height: 50%;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col" style="width: 50%;">Name</th>
						<th scope="col" style="width: 50%;">Price</th>
					</tr>
				</thead>
				<tbody v-if="show">
					<tr v-if="!exists" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
					  	<td colspan="5">There is no virtual machines meeting the criteria.</td>
					</tr>
					<tr v-for="c in components.vms">
						<td>{{c.vmName}}</td>
						<td>{{c.price}}€</td>
					</tr>
					<tr v-for="d in components.drives">
						<td>{{d.driveName}}</td>
						<td>{{d.price}}€</td>
					</tr>
					<tr class="table-primary">
						<td>Total:</td>
						<td>{{components.totalCost}}€</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>

		<div class="card" style="height: 100%">
			
		</div>
  </div>
	`,
	methods: {
		getReceipt: function() {
			if(this.korisnik.uloga == "ADMIN"){
				if(this.dateFrom == null || this.dateFrom == "") {
					document.getElementById("dateFrom").classList.add("is-invalid");
				} else {
					document.getElementById("dateFrom").classList.remove("is-invalid");
				}
				
				if(this.dateTo == null || this.dateTo == "") {
					document.getElementById("dateTo").classList.add("is-invalid");
				} else {
					document.getElementById("dateTo").classList.remove("is-invalid");
				}
				
				if(this.dateFrom != null && this.dateFrom != "" && this.dateTo != null && this.dateTo != "") {
					axios
					.get('/main/org/getReceipt', {
						params:{
							datumPaljenja:this.dateFrom,
							datumGasenja:this.dateTo
						}
	
					})
					.then(response => {
						this.components = response.data;
						if(this.components.vms.length > 0 || this.components.drives.length > 0){ 
							this.success = true;
							this.show = true;
							this.exists = true;
						}
						else{
							this.success = false;
							this.show = true;
							this.exists = false;
						}
					});
				}
			}
		}
	},
	mounted () {
		axios
        .get('main/session/getUser')
        .then(response => {
       	 this.korisnik = response.data;
        });
	},
});