Vue.component ("user-view", {
	data: function () {
		return {
			korisnik: '',
			validEMAIL: true,
			success: true,
			name: null,
			lastname: null,
			pass: null,
			type: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>

		<div class="card text-center col-md-8" style="float: left;">
			<div class="card-header">
				<big>User info</big>
			</div>
			<div class="card-body" style="align-self: center;">
				<div class="mb-3">
					<img src="./images/user.png" class="rounded-circle user-image">
				</div>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Name
						<h4>
							
							<span class="badge badge-light">{{korisnik.ime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Lastname
						<h4>
							<span class="badge badge-light">{{korisnik.prezime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Email
						<h4>
							<span class="badge badge-light">{{korisnik.email}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Role
						<h4>
							<span class="badge badge-light">{{korisnik.uloga}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Organization
						<h4>
							<span class="badge badge-light">{{korisnik.organizacija}}</span>
						</h4>
					</li>
				</ul>
			</div>
		</div>

		<div class="card" style="height: 100%">
			<div class="card-header">
				<big>Edit info</big>
			</div>
			<div class="card-body col-md-12" style="float: right; height: 100%">

					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>Lastname</label> <input type="text"
							class="form-control" id="lastname" v-model="lastname" placeholder="Lastname">
					</div>
					<div class="form-group col-md-12">
						<label>Password</label> <input type="password"
							class="form-control" id="pass" v-model="pass" placeholder="Password">
					</div>
					
					<div class="form-group col-md-12">
						<label>Type</label>
						<select class="custom-select" id="type" name="type" v-model="type">
							<option>Admin</option>
							<option>Korisnik</option>
						 </select>
					</div>
			  		
			  		<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  			<strong>Error!</strong> User with this email already exists.
					</div>
					<div v-if="!validEMAIL" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  			<strong>Error!</strong> Email form is not valid.
					</div>
					
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="editUser()">Submit</button>
					</div>
					<hr>
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="deleteUser()">Delete</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		validEmail: function(email) {
		      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		      return re.test(email);
		},
		editUser: function() {
			if(this.korisnik_sesija.uloga != "KORISNIK"){
				
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.lastname == null || this.lastname == "") {
					document.getElementById("lastname").classList.add("is-invalid");
					document.getElementById("lastname").placeholder = "Mandatory!";
				} else {
					document.getElementById("lastname").classList.remove("is-invalid");
				}
				
				if(this.pass == null || this.pass == "") {
					document.getElementById("pass").classList.add("is-invalid");
					document.getElementById("pass").placeholder = "Mandatory!";
				} else {
					document.getElementById("pass").classList.remove("is-invalid");
				}
				
				if(this.type == null || this.type == "") {
					document.getElementById("type").classList.add("is-invalid");
				} else {
					document.getElementById("type").classList.remove("is-invalid");
				}
				
			
				if(this.name != "" && this.name != null && this.lastname != null && this.lastname != ""
					&& this.pass != null && this.pass != "" && this.type != null && this.type != "") {
					axios
					.post('/main/org/editUser/' + this.korisnik.email,  {
						"email":this.korisnik.email,
						"name":this.name,
						"lastname":this.lastname,
						"password":this.pass,
						"uloga":this.type
						})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/user-view/' + this.korisnik.email);
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		},
		deleteUser: function() {
			if(this.korisnik_sesija.uloga != "KORISNIK"){
				axios
				.post('/main/org/deleteUser/' + this.korisnik.email)
				.then(response => {
					if(response.data === "SUCCESS") {
						this.$router.push('/users');
					} else {
						console.log("ERROR");
					}
				});
			}
		}
	},
	
	mounted () {
        axios
        .get('/main/org/getUser/' + this.$route.params.user_id)
        .then(response => {
      	  this.korisnik = response.data;
        });
        axios
        .get('/main/session/getUser')
        .then(response => {
      	  this.korisnik_sesija = response.data;
        });
        
    },
});