Vue.component ("category-view", {
	data: function () {
		return {
			kategorija: null,
			success: true,
			attached: false,
			korisnik: '',
			name: null,
			cpu: null,
			ram: null,
			gpu: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>

		<div class="card text-center col-md-8" style="float: left;">
			<div class="card-header">
				<big>Category info</big>
			</div>
			<div class="card-body">
				<div class="mb-3">
					<img src="./images/category.png" class="user-image">
				</div>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Name
						<h4>
							
							<span class="badge badge-light">{{kategorija.ime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						CPU count
						<h4>
							<span class="badge badge-light">{{kategorija.brojJezgara}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						RAM
						<h4>
							<span class="badge badge-light">{{kategorija.RAM}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						GPU count
						<h4>
							<span class="badge badge-light">{{kategorija.brojGPUJezgara}}</span>
						</h4>
					</li>
				</ul>
			</div>
		</div>

		<div class="card" style="height: 100%">
			<div class="card-header">
				<big>Edit category</big>
			</div>
			<div class="card-body col-md-12" style="float: right; height: 100%">

					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>CPU count</label> <input type="number"
							class="form-control" id="cpu" v-model="cpu" placeholder="CPU count">
					</div>
					<div class="form-group col-md-12">
						<label>RAM</label> <input type="number"
							class="form-control" id="ram" v-model="ram" placeholder="RAM">
					</div>
					
					<div class="form-group col-md-12">
						<label>GPU count</label>
						<input type="number" class="form-control" id="gpu" v-model="gpu" placeholder="GPU count">
					</div>
			  		
			  		<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  			<strong>Error!</strong> Category with this name already exists.
					</div>
					
					<div v-if="attached" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  			<strong>Error!</strong> Category is attached to virtual machine.
					</div>
				
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="editCategory()">Submit</button>
					</div>
					<hr>
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="deleteCategory()">Delete</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		editCategory: function() {
			if(this.korisnik.uloga === "SUPER_ADMIN") {
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.cpu == null || this.cpu == "") {
					document.getElementById("cpu").classList.add("is-invalid");
					document.getElementById("cpu").placeholder = "Mandatory!";
				} else {
					document.getElementById("cpu").classList.remove("is-invalid");
				}
				
				if(this.ram == null || this.ram == "") {
					document.getElementById("ram").classList.add("is-invalid");
					document.getElementById("ram").placeholder = "Mandatory!";
				} else {
					document.getElementById("ram").classList.remove("is-invalid");
				}
				
				if(this.gpu == null || this.gpu == "") {
					document.getElementById("gpu").classList.add("is-invalid");
					document.getElementById("gpu").placeholder = "Mandatory!";
				} else {
					document.getElementById("gpu").classList.remove("is-invalid");
				}
				
			
				if(this.name != "" && this.name != null && this.cpu != null && this.cpu != "" && this.ram != null && this.ram != ""
					&& this.gpu != null && this.gpu != "") {
					axios
					.post('/main/org/editCategory/' + this.kategorija.ime,  {
						"ime":this.name,
						"brojJezgara":this.cpu,
						"RAM":this.ram,
						"brojGPUJezgara":this.gpu
						})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/category-view/' + this.name);
							location.reload();
						} else{
							this.success = false;
						}
					});
				}
			}
		},
		deleteCategory: function() {
			if(this.korisnik.uloga === "SUPER_ADMIN"){
				axios
					.post('/main/org/deleteCategory/' + this.kategorija.ime)
					.then(response => {
						if(response.data === "SUCCESS") {
							this.attached = false;
							this.success = true;
							this.$router.push('/category');
						} else if(response.data === "ATTACHED"){
							this.attached = true;
							this.success = true;
						} else {
							this.attached = false;
							this.success = false;
						}
				});
			}
		}
	},
	
	mounted () {
        axios
	        .get('/main/org/getKategorija/' + this.$route.params.cat_id)
	        .then(response => {
	      	  this.kategorija = response.data;
	        })
	        .catch(error => {
	        	console.log("403 - Allowed only for SUPER_ADMIN.");
        });
        axios
	        .get('main/session/getUser')
	        .then(response => {
	       	 this.korisnik = response.data;
        });
    },
});