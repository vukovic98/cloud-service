Vue.component ("category", {
	data: function () {
		return {
			kategorije: null,
			korisnik: null,
			name: null,
			gpu: null,
			cpu: null,
			ram: null,
			success: true,
			exists: true
		}
	},
	template: `
	<div>
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="col-md-8 vm-table">
		<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2" style="height: 50%;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">Name</th>
						<th scope="col">CPU count</th>
						<th scope="col">RAM</th>
						<th scope="col">GPU</th>
					</tr>
				</thead>
				<tbody>
					<tr v-if="!exists" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
					  	<td colspan="5">There is no categories in system.</td>
					</tr>
					<tr v-for="k in kategorije" v-on:click="go(k.ime)">
						<td>{{k.ime}}</td>
						<td>{{k.brojJezgara}}</td>
						<td>{{k.RAM}}</td>
						<td>{{k.brojGPUJezgara}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>

		<div class="card" style="height: 100%">
			<div class="card-header">
				<big>Add category</big>
			</div>
			<div class="card-body col-md-12 pl-2 pr-2" style="float: right; height: 100%">
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>CPU count</label> <input type="number"
							class="form-control" id="cpu" v-model="cpu" placeholder="CPU count">
					</div>
					<div class="form-group col-md-12">
						<label>RAM</label> <input type="number"
							class="form-control" id="ram" v-model="ram" placeholder="RAM">
					</div>
					<div class="form-group col-md-12">
						<label>GPU count</label> <input type="number"
							class="form-control" id="gpu" v-model="gpu" placeholder="GPU count">
					</div>

			  		<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error! </strong>Category already exists!
			  		</div>
					<div class="align-items-center col-md-12 mt-3">
						<button type="submit" class="btn btn-primary btn-lg btn-block" v-on:click="add()">Submit</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		go: function(ime) {
			if(this.korisnik.uloga === "SUPER_ADMIN"){
				this.$router.push("/category-view/" + ime);
			}
		},
		add: function() {
			if(this.korisnik.uloga === "SUPER_ADMIN") {
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				}else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.cpu == null || this.cpu == "") {
					document.getElementById("cpu").classList.add("is-invalid");
					document.getElementById("cpu").placeholder = "Mandatory!";
				}else {
					document.getElementById("cpu").classList.remove("is-invalid");
				}
				
				if(this.ram == null || this.ram == "") {
					document.getElementById("ram").classList.add("is-invalid");
					document.getElementById("ram").placeholder = "Mandatory!";
				}else {
					document.getElementById("ram").classList.remove("is-invalid");
				}
				
				if(this.gpu == null || this.gpu == "") {
					document.getElementById("gpu").classList.add("is-invalid");
					document.getElementById("gpu").placeholder = "Mandatory!";
				}else {
					document.getElementById("gpu").classList.remove("is-invalid");
				}
				
				if(this.name != null && this.name != "" && this.cpu != null && this.cpu != "" && this.ram != null && this.ram != ""
					&& this.gpu != null && this.gpu != "") {
					axios
					.post('main/org/addCategory', {
						"ime":this.name,
						"brojJezgara":this.cpu,
						"RAM":this.ram,
						"brojGPUJezgara":this.gpu
					})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/category');
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		}
	},	
	mounted () {
        axios
          .get('main/org/getKategorije')
          .then(response => {
        	 this.kategorije = response.data; 
        	 if(this.kategorije.length == 0) {
        		 this.exists = false;
        	 } else {
        		 this.exists = true;
        	 }
          })
          .catch(error => {
        	 console.log("403 - Allowed only to SUPER_ADMIN."); 
          });
        axios
	        .get('/main/session/getUser')
	        .then(response => {
	      	  this.korisnik = response.data;
        });
    },
});