Vue.component ("machine", {
	data: function () {
		return {
			virtuelnaMasina: null,
			organizacija: null,
			sveOrganizacije: null,
			sveKategorije: null,
			korisnik: '',
			name: null,
			org: null,
			kat: null,
			success: true,
			turnedOn: false,
			drives: null,
			choosenDrives: []
		}
	},
	template: `
	<div>
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>

		<div class="card text-center col-md-8" style="float: left;">
			<div class="card-header">
				<big>Machine info</big>
			</div>
			<div class="card-body">
				<div class="mb-3">
					<img src="./images/vm.png" class="rounded-circle user-image">
				</div>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Name
						<h4>
							<span class="badge badge-light">{{virtuelnaMasina.ime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						CPU
						<h4>
							<span class="badge badge-light">{{virtuelnaMasina.kategorija.brojJezgara}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						GPU
						<h4>
							<span class="badge badge-light">{{virtuelnaMasina.kategorija.brojGPUJezgara}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						RAM
						<h4>
							<span class="badge badge-light">{{virtuelnaMasina.kategorija.RAM}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Organization
						<h4>
							<span class="badge badge-light">{{virtuelnaMasina.organizacija}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Category
						<h4>
							<span class="badge badge-light">{{virtuelnaMasina.kategorija.ime}}</span>
						</h4>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						<template v-if="turnedOn">
							Machine is ON
							<h2>
								<button type="button" class="btn btn-danger" v-on:click="switch_status()">Turn off</button>
							</h2>
						</template>
						<template v-if="!turnedOn">
							Machine is OFF
							<h2 v-if="korisnik.uloga != 'KORISNIK'">
								<button type="button" class="btn btn-success" v-on:click="switch_status()">Turn on</button>
							</h2>
						</template>
					</li>
				</ul>
				<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2">

				  <table class="table table-bordered table-sm table-striped mb-0">
				    <thead>
				      <tr>
				        <th scope="col"><big>Activity</big></th>
				        <th scope="col">Date from</th>
				        <th scope="col">Date to</th>
				        <th v-if="korisnik.uloga == 'SUPER_ADMIN'" scope="col">
				        	<div class="align-items-right col-md-12">
								<add-act></add-act>
							</div>
				        </th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr v-for="a in virtuelnaMasina.aktivnosti">
				      	<td></td>
				      	<td>{{a.datumPaljenja}}</td>
				      	<td>{{a.datumGasenja}}</td>
				      	<td v-if="korisnik.uloga == 'SUPER_ADMIN'">
				      		<div class="align-items-right col-md-12">
								<button type="button" class="btn btn-outline-danger" v-on:click="deleteAct(a.datumPaljenja)">Delete</button>
							</div>
				      	</td>
				      </tr>
				    </tbody>
				  </table>
				
				</div>
			</div>
		</div>

		<div class="card" style="height: 100%">
			<div  v-if="korisnik.uloga != 'KORISNIK'" class="card-header">
				<big>Edit VM</big>
			</div>
			<div  v-if="korisnik.uloga != 'KORISNIK'" class="card-body col-md-12" style="float: right; height: 100%">
				<form>

					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-row col-md-12">
						<div class="form-group col-md-12">
							<label>Category</label> 
							<select id="kat" v-model="kat" class="form-control">
								<option  disabled selected hidden>Choose</option>
								<template v-for="k in sveKategorije">
									<option>{{k.ime}}</option>
								</template>
							</select>
						</div>
					</div>
					<div class="form-row col-md-12">
						<div class="form-group col-md-12">
							<label>Drives</label> 
							<select class="custom-select" id="choosenDrives" name="choosenDrives" multiple v-model="choosenDrives" style="max-height: 105px;">
								<template v-for="d in drives">
							    	<option>{{d.ime}}</option>
							    </template>
							 </select>
						</div>
					</div>

							
					<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  			<strong>Error!</strong> Virtual machine with this name already exists.
					</div>
					
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="editVM()">Submit</button>
					</div>
					<hr>
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="deleteVM()">Delete</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	`,
	methods: {
		editVM: function() {
			if(this.korisnik.uloga != 'KORISNIK'){
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Name must be inserted!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}

				if(this.kat == null || this.kat == "") {
					document.getElementById("kat").classList.add("is-invalid");
				} else {
					document.getElementById("kat").classList.remove("is-invalid");
				}
			
				if(this.name != "" && this.name != null  && this.kat != null && this.kat != "") {
					axios
					.post('/main/org/editVM/' + this.$route.params.id,  {
						"vm_ime":this.name,
						"kat":this.kat,
						"org":null,
						"drives":this.choosenDrives
						})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							console.log(this.ime + "SUCCESS");
							this.$route.params.id = this.name;
							this.$router.push('/machine/' + this.name);
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		},
		deleteVM: function() {
			if(this.korisnik.uloga != 'KORISNIK'){
				axios
				.post('/main/org/deleteVM/' + this.virtuelnaMasina.ime)
				.then(response => {
					if(response.data === "SUCCESS") {
						this.$router.push('/vmpage');
					} else {
						console.log("ERROR");
					}
				});
			}
		},
		deleteAct: function(datumPaljenja) {
			if(this.korisnik.uloga == "SUPER_ADMIN"){
				axios
				.post('/main/org/deleteAct/' + this.virtuelnaMasina.ime + '/' + datumPaljenja)
				.then(response => {
					if(response.data === "SUCCESS") {
						this.$router.push('/machine/' + this.name);
						location.reload();
					} else {
						console.log("ERROR");
					}
				});
			}
		},
		switch_status: function() {
			if(this.korisnik.uloga != 'KORISNIK'){
				axios
				.post('/main/org/switchVM/' + this.virtuelnaMasina.ime)
				.then(response => {
					if(response.data === "SUCCESS") {
						this.$router.push('/machine/' + this.virtuelnaMasina.ime);
						location.reload();
					}
				});
			}
		},
		loadDrives: function() {
			axios
			.get('/main/org/getEditDiskovi/' + this.virtuelnaMasina.ime)
			.then(response => {
				this.drives = response.data;
			});
		}
	},
	
	mounted () {
        axios
          .get('main/org/getVM/' + this.$route.params.id)
          .then(response => {
        	  console.log(this.$route.params.id);
        	  this.virtuelnaMasina = response.data;
        	  this.name = this.virtuelnaMasina.ime;
        	  this.loadDrives();
          });
        axios
        .get('/main/org/getOrganizacije')
	        .then(response => {
	      	  this.sveOrganizacije = response.data;
	        })
	        .catch(error => {
	        	console.log("403 - Forbidden for USER.");
	        });
        axios
	        .get('/main/org/getKategorije')
	        .then(response => {
	        	this.sveKategorije = response.data;
	        })
	        .catch(error => {
	        	console.log("403 - Forbidden for USER.");
	        });
        axios
        .get('/main/session/getUser')
        .then(response => {
      	  this.korisnik = response.data;
        });
        axios
	        .get('/main/org/activeVM/' + this.$route.params.id)
	        .then(response => {
	      	  if(response.data === "ON") {
	      		  this.turnedOn = true;
	      	  } else {
	      		  this.turnedOn = false;
	      	  }
	        })
	        .catch(error => {
	        	console.log("403 - Forbidden for user!");
        });
	},
});