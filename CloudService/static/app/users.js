Vue.component ("users", {
	data: function () {
		return {
			korisnik: '',
			validEMAIL: true,
			korisnici: null,
			name: null,
			lastname: null,
			email: null,
			pass: null,
			org: null,
			type: null,
			success: true,
			organizacije: null
		}
	},
	template: `
	<div style="height: 100%">
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="col-md-8 vm-table">
			<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2"  style="height: 50%;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">Email</th>
						<th scope="col">Name</th>
						<th scope="col">Lastname</th>
						<th scope="col">Organization</th>
					</tr>
				</thead>
				<tbody>
					<tr v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
					  	<td colspan="5">There are no users.</td>
					</tr>
					<tr v-for="k in korisnici" v-on:click="go(k.email)">
						<td>{{k.email}}</td>
						<td>{{k.ime}}</td>
						<td>{{k.prezime}}</td>
						<td>{{k.organizacija}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		
		<div class="card" style="height: 100%">
			<div  v-if="korisnik.uloga != 'KORISNIK'" class="card-header">
				<big>Add user</big>
			</div>
			<div  v-if="korisnik.uloga != 'KORISNIK'" class="card-body col-md-12 pl-2 pr-2" style="float: right; height: 100%">
					<div class="form-group col-md-12">
						<label>Email</label> <input type="text"
							class="form-control" id="email" v-model="email" placeholder="email@example.com">
					</div>
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>Lastname</label> <input type="text"
							class="form-control" id="lastname" v-model="lastname" placeholder="Lastname">
					</div>
					<div class="form-group col-md-12">
						<label>Password</label> <input type="password"
							class="form-control" id="pass" v-model="pass" placeholder="Password">
					</div>
					<div class="form-group col-md-12">
						<label>Organization</label> 
						<select name="org" id="org" v-model="org" class="form-control">
							<option  disabled selected hidden>Choose</option>
							<template v-for="o in organizacije">
								<option>{{o.ime}}</option>
							</template>
						</select>
					</div>
					<div class="form-group col-md-12">
						<label>Type</label> 
						<select name="type" id="type" v-model="type" class="form-control">
							<option  disabled selected hidden>Choose</option>
							<option>Admin</option>
							<option>Korisnik</option>
							</template>
						</select>
					</div>

			  		<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>User already exists!
			  		</div>
			  		
			  		<div v-if="!validEMAIL" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong> Email form is not valid.
			  		</div>
			  		
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="add()">Submit</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		validEmail: function(email) {
		      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		      return re.test(email);
		},
		add: function() {
			if(this.korisnik.uloga != 'KORISNIK'){
				if(this.email == null || this.email == "") {
					document.getElementById("email").classList.add("is-invalid");
					document.getElementById("email").placeholder = "Mandatory!";
				} else {
					document.getElementById("email").classList.remove("is-invalid");
				}
				
				if(!this.validEmail(this.email)) {
					this.validEMAIL = false;
					document.getElementById("email").classList.add("is-invalid");
				} else {
					this.validEMAIL = true;
					document.getElementById("email").classList.remove("is-invalid");
				}
				
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.lastname == null || this.lastname == "") {
					document.getElementById("lastname").classList.add("is-invalid");
					document.getElementById("lastname").placeholder = "Mandatory!";
				} else {
					document.getElementById("lastname").classList.remove("is-invalid");
				}
				
				if(this.pass == null || this.pass == "") {
					document.getElementById("pass").classList.add("is-invalid");
					document.getElementById("pass").placeholder = "Mandatory!";
				} else {
					document.getElementById("pass").classList.remove("is-invalid");
				}
				
				if(this.org == null || this.org == "") {
					document.getElementById("org").classList.add("is-invalid");
				} else {
					document.getElementById("org").classList.remove("is-invalid");
				}
				
				if(this.type == null || this.type == "") {
					document.getElementById("type").classList.add("is-invalid");
				} else {
					document.getElementById("type").classList.remove("is-invalid");
				}
				
				if(this.name != "" && this.name != null && this.email != null && this.email != "" && this.lastname != null && this.lastname != ""
					&& this.pass != null && this.pass != "" && this.org != null && this.org != "" && this.type != null && this.type != "" && this.validEMAIL) {
					axios
					.post('/main/org/addUser',  {
						"email":this.email,
						"password":this.pass,
						"name":this.name,
						"lastname":this.lastname,
						"organizacija":this.org,
						"uloga":this.type
						})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/users');
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		},
		go: function(k_email) {
			if(this.korisnik.uloga != 'KORISNIK' && this.korisnik.email != k_email){
				this.$router.push("/user-view/" + k_email);
			}
		}
	},
	mounted () {
		axios
        .get('/main/session/getUser')
        .then(response => {
      	  	this.korisnik = response.data;
        })
        .catch(error => {
        	console.log("403 - Forbidden for USER!");
        });
		
        axios
        .get('/main/org/getUsers')
        .then(response => {
        	this.korisnici = response.data;
        })
        .catch(error => {
        	console.log("403 - Forbidden for USER!");
        });
        
        axios
        .get('/main/org/getOrganizacije')
        .then(response =>{
        	this.organizacije = response.data;
        })
        .catch(error => {
        	console.log("403 - Forbidden for USER!");
        });
    },
});