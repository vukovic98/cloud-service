Vue.component ("drives", {
	data: function () {
		return {
			organizacije: null,
			virtuelneMasine: null,
			virtuelnaMasina: null,
			drives: null,
			korisnik: null,
			show: true,
			exists: true,
			success: true,
			kat: null,
			org: null,
			cap: null,
			name: null,
			tip: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="col-md-8 vm-table">
		<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2" style="height: 50%;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col" class="third-width">Name</th>
						<th scope="col" class="third-width">Capacity</th>
						<th scope="col" class="third-width">Virtual machine</th>
					</tr>
				</thead>
				<tbody>
					<tr v-if="!exists" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
					  	<td colspan="5">There is no drives meeting the criteria.</td>
					</tr>
					<tr v-for="d in drives" v-on:click="go(d.ime)">
						<td>{{d.ime}}</td>
						<td>{{d.kapacitet}}</td>
						<td>{{d.virtuelnaMasina}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>

		<div class="card" style="height: 100%">
			<div  v-if="korisnik.uloga != 'KORISNIK'" class="card-header">
				<big>Add drive</big>
			</div>
			<div  v-if="korisnik.uloga != 'KORISNIK'" class="card-body col-md-12 pl-2 pr-2" style="float: right; height: 100%">
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" name="name" v-model="name" placeholder="Name">
					</div>
					
					<div class="form-row col-md-12">
						<div class="form-group col-md-12">
							<label>Type</label> 
							<select id="tip" v-model="tip" class="form-control">
								<option  disabled selected hidden>Choose</option>
								<option>HDD</option>
								<option>SSD</option>
							</select>
						</div>
					</div>
					
					<div class="form-group col-md-12">
						<label>Capacity</label> <input type="number"
							class="form-control" id="cap" name="cap" v-model="cap" placeholder="Capacity">
					</div>
					<div class="form-row col-md-12">
						<div class="form-group col-md-12">
							<label>Organization</label> 
							<select id="org" v-model="org" class="form-control" v-on:change="inMachines">
								<option  disabled selected hidden>Choose</option>
								<template v-for="o in organizacije">
									<option>{{o.ime}}</option>
								</template>
							</select>
						</div>
					</div>

					<div v-if="show" class="form-row col-md-12">
						<div class="form-group col-md-12">
							<label>Virtual machine</label> 
							<select id="virtuelnaMasina" v-model="virtuelnaMasina" class="form-control">
								<option  disabled selected hidden>Choose</option>
								<template v-for="v in virtuelneMasine">
									<option>{{v.ime}}</option>
								</template>
							</select>
						</div>
					</div>
					
			  		<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>Drive already exists!
			  		</div>
					<div class="align-items-center col-md-12 mt-3">
						<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="addDrive()">Submit</button>
					</div>
			</div>
		</div>
	</div>
	`,
	methods: {
		addDrive: function() {
			if(this.korisnik.uloga != 'KORISNIK'){
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.cap == null || this.cap == "") {
					document.getElementById("cap").classList.add("is-invalid");
					document.getElementById("cap").placeholder = "Mandatory!";
				} else {
					document.getElementById("cap").classList.remove("is-invalid");
				}
				
				if(this.org == null || this.org == "") {
					document.getElementById("org").classList.add("is-invalid");
				} else {
					document.getElementById("org").classList.remove("is-invalid");
				}
					
					
				if(this.name != null && this.name != "" && this.cap != null & this.cap != "" && this.org != null && this.org != "") {
					axios
					.post('/main/org/addDrive', {
						"ime":this.name,
						"organizacija":this.org,
						"tip":this.tip,
						"kapacitet":this.cap,
						"virtuelnaMasina":this.virtuelnaMasina
					})
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/drives');
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		},
		inMachines: function() {
			if(this.korisnik.uloga != "KORISNIK"){
				axios
				.get('/main/org/getVirtualMachines/' + this.org)
				.then(response => {
					this.virtuelneMasine = response.data;
					this.show = true;
				});
			}
		},
		go: function(ime) {
			this.$router.push("/drive-view/" + ime);
		}
	},
	
	mounted () {
        axios
		  .get('main/org/getOrganizacije')
		  .then(response => {
			  this.organizacije = response.data;
		  })
		  .catch(error => {
			 console.log("403 - Forbidden for USER!"); 
		  });
        axios
         .get('main/session/getUser')
         .then(response => {
        	 this.korisnik = response.data;
         });
        axios
        .get('main/org/getDrives')
        .then(response => {
       	 	this.drives = response.data;
        });
    },
});