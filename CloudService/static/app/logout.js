Vue.component("logout", {
    template: `
<p>LOGGING OUT...</p>
`,
    mounted(){
		axios
		.get('/main/logout')
		.then(response => (
				this.$router.push('/login')
		));
    }

});