Vue.component ("vm-page", {
	data: function () {
		return {
			virtuelneMasine: null,
			kategorije: null,
			vmName: null,
			CPU_od: null,
			CPU_do: null,
			GPU_od: null,
			GPU_do: null,
			RAM_od: null,
			RAM_do: null,
			korisnik: '',
			exists: true
		}
	},
	template: `
	<div>
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="col-md-8 vm-table">
		<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2" style="height: 50%;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">Name</th>
						<th scope="col">CPU count</th>
						<th scope="col">RAM</th>
						<th scope="col">GPU</th>
						<th scope="col">Organization</th>
					</tr>
				</thead>
				<tbody>
					<tr v-if="!exists" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
					  	<td colspan="5">There is no virtual machines meeting the criteria.</td>
					</tr>
					<tr v-for="i in virtuelneMasine" v-on:click="go(i.ime)">
						<td>{{i.ime}}</td>
						<td>{{i.kategorija.brojJezgara}}</td>
						<td>{{i.kategorija.RAM}}</td>
						<td>{{i.kategorija.brojGPUJezgara}}</td>
						<td>{{i.organizacija}}</td>
					</tr>
					
				</template>
				</tbody>
			</table>
		</div>
		</div>

		<div class="col-md-2 order-md-2 vm-sidebar">
			<ul class="list-group mb-3">
				<li class="list-group-item d-flex justify-content-between lh-condensed">
					<div>
						<h6 class="my-0 pb-3">CPU count</h6>
						<div class="col-auto">
							<div class="input-group mb-2">
								<div class="input-group-prepend">
									<div class="input-group-text">From</div>
								</div>
								<input type="number" class="form-control" id="CPU_od" v-model="CPU_od">
							</div>
						</div>

						<div class="col-auto">
							<div class="input-group mb-2">
								<div class="input-group-prepend">
									<div class="input-group-text">To</div>
								</div>
								<input type="number" class="form-control" id="CPU_do" v-model="CPU_do">
							</div>
						</div>
					</div>

				</li>


				<li class="list-group-item d-flex justify-content-between lh-condensed">
						<div>
							<h6 class="my-0 pb-3">RAM</h6>
							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">From</div>
									</div>
									<input type="number" class="form-control" id="RAM_od" v-model="RAM_od">
								</div>
							</div>

							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">To</div>
									</div>
									<input type="number" class="form-control" id="RAM_do" v-model="RAM_do">
								</div>
							</div>
						</div>
				</li>


				<li class="list-group-item d-flex justify-content-between lh-condensed">
						<div>
							<h6 class="my-0 pb-3">GPU</h6>
							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">From</div>
									</div>
									<input type="number" class="form-control" id="GPU_od" v-model="GPU_od">
								</div>
							</div>

							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">To</div>
									</div>
									<input type="number" class="form-control" id="GPU_do" v-model="GPU_do">
								</div>
							</div>
						</div>
				</li>

				<li class="list-group-item d-flex justify-content-between lh-condensed">
						<div>
							<h6 class="my-0 pb-3">Search by name</h6>
							<div class="col-auto">

								<input type="text" class="form-control" id="vmName" v-model="vmName">

							</div>
						</div>
				</li>
				<div style="margin-top: 20%">
					<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="search()">Search</button>
				</div>
				<div v-if="korisnik.uloga != 'KORISNIK'" style="margin-top: 5%">
					<add-vm></add-vm>
				</div>
			</ul>
		</div>
	</div>
	`,
	methods: {
		search: function() {
				axios
				.post('/main/org/search', {
					"vmName":this.vmName,
					"CPU_od":this.CPU_od,
					"CPU_do":this.CPU_do,
					"RAM_od":this.RAM_od,
					"RAM_do":this.RAM_do,
					"GPU_od":this.GPU_od,
					"GPU_do":this.GPU_do})
				.then(response => {
					this.virtuelneMasine = response.data;
					if(this.virtuelneMasine.length === 0) {
						this.exists = false;
					} else {
						this.exists = true;
					}
				});
		},
		go: function(ime) {
			this.$router.push("/machine/" + ime);
		}
	},
	created () {
		axios
	        .get('main/session/getUser')
	        .then(response => {
	       	 this.korisnik = response.data;
        });
	},
	mounted () {
		axios
          .get('main/org/getVMasine')
          .then(response => {
        	  this.virtuelneMasine = response.data;
        	  
          });
		if(this.korisnik.uloga != 'KORISNIK'){
        axios
          .get('main/org/getKategorije')
          .then(response => {       	  
       		  this.kategorije = response.data; 
          })
          .catch(error => {
        	  console.log("403");
          });
		}
    },
});