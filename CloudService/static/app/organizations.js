Vue.component ("organizations", {
	data: function () {
		return {
			organizacije: null,
			korisnik: '',
			exists: true,
			success: true,
			name: null,
			desc: null,
			i_file: null,
			virtuelneMasine: null,
			diskovi: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 order-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>
		<div class="col-md-8 vm-table">
		<div class="table-wrapper-scroll-y my-custom-scrollbar mt-2" style="height: 50%;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col" class="third-width">Name</th>
						<th scope="col" class="third-width">Description</th>
						<th scope="col" class="third-width">Logo</th>
					</tr>
				</thead>
				<tbody>
					<tr v-if="!exists" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
					  	<td colspan="5">There is no virtual machines meeting the criteria.</td>
					</tr>
					<tr v-for="o in organizacije" v-on:click="go(o.ime)">
						<td>{{o.ime}}</td>
						<td>{{o.opis}}</td>
						<td><img v-bind:src="o.logo" class="rounded-circle user-image" style="width: 45px; height: 45px;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>

		<div class="card" style="height: 100%">
			<div  v-if="korisnik.uloga == 'SUPER_ADMIN'" class="card-header">
				<big>Add organization</big>
			</div>
			<div  v-if="korisnik.uloga == 'SUPER_ADMIN'" class="card-body col-md-12 pl-2 pr-2" style="float: right; height: 100%">
			<form method="POST" ref="addOrg" v-on:submit="addOrg">
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" name="name" v-model="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>Description</label> <input type="text"
							class="form-control" id="desc" name="desc" v-model="desc" placeholder="Description">
					</div>
					<div class="col-md-12">
						<label>Logo</label>
						<div class="input-group mb-3">
						  <div class="custom-file">
						    <input type="file" accept="image/*" class="custom-file-input" name="i_file" id="i_file" onchange="document.getElementById('lab').innerHTML='Selected'">
						    <label class="custom-file-label" for="i_file" id="lab" >Choose</label>
						  </div>
						</div>
					</div>
					
			  		<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>Organization already exists!
			  		</div>
					<div v-if="korisnik.uloga == 'SUPER_ADMIN'" class="align-items-center col-md-12 mt-3">
						<button type="submit" class="btn btn-primary btn-lg btn-block" v-on:click="addOrg()">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	`,
	methods: {
		addOrg: function(event) {
			event.preventDefault();
			if(this.korisnik.uloga === 'SUPER_ADMIN'){
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.desc == null || this.desc == "") {
					document.getElementById("desc").classList.add("is-invalid");
					document.getElementById("desc").placeholder = "Mandatory!";
				} else {
					document.getElementById("desc").classList.remove("is-invalid");
				}
				
				this.i_file = document.getElementById("i_file").value;
				if(this.i_file == null || this.i_file == "") {
					document.getElementById("i_file").classList.add("is-invalid");
				} else {
					document.getElementById("i_file").classList.remove("is-invalid");
				}
					
					
				if(this.name != null && this.name != "" && this.desc != null & this.desc != "" && this.i_file != null && this.i_file != "") {
					
					let formData = new FormData(this.$refs.addOrg)
					console.log(this.i_file);
					axios
					.post('/main/org/addOrg', formData)
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/organizations');
							location.reload();
						} else {
							this.success = false;
						}
					});
				}
			}
		},
		go: function(ime) {
			if(this.korisnik.uloga != 'KORISNIK'){
				this.$router.push("/organization-view/" + ime);
			}
		}
	},
	
	mounted () {
        axios
          .get('main/org/getOrganizacije')
          .then(response => {
        	  this.organizacije = response.data;
          })
          .catch(error => {
        	 console.log("403 - Forbidden for USER."); 
          });
        axios
         .get('main/session/getUser')
         .then(response => {
        	 this.korisnik = response.data;
         });
    },
});