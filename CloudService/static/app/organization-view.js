Vue.component ("organization-view", {
	data: function () {
		return {
			organizacija: null,
			success: true,
			name: null,
			korisnik: '',
			desc: null,
			i_file: null
		}
	},
	template: `
	<div>
		<div class="col-md-2 mb-4 vm-sidebar">
			<sidebar></sidebar>
		</div>

		<div class="card text-center col-md-8" style="float: left;">
			<div class="card-header">
				<big>Organization info</big>
			</div>
			<div class="card-body">
				<div class="mb-3">
					<img v-bind:src="organizacija.logo" class="user-image">
				</div>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Name
						<h4>
							
							<span class="badge badge-light">{{organizacija.ime}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						Description
						<h4>
							<span class="badge badge-light">{{organizacija.opis}}</span>
						</h4>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						<div class=" col-md-2">Virtual machines</div>
						<div class="table-wrapper-scroll-y my-custom-scrollbar table-sm mt-2 col-md-10" style="height: 100px">
						<table class="table table-hover pl-2 pr-2">
							<thead>
								<tr>
									<th scope="col">Name</th>
									<th scope="col">CPU count</th>
									<th scope="col">RAM</th>
									<th scope="col">GPU</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="v in organizacija.virtuelneMasine">
									<td>{{v.ime}}</td>
									<td>{{v.kategorija.brojJezgara}}</td>
									<td>{{v.kategorija.RAM}}</td>
									<td>{{v.kategorija.brojGPUJezgara}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						<div class=" col-md-2">Drives</div>
						<div class="table-wrapper-scroll-y table-sm my-custom-scrollbar mt-2 col-md-10" style="height: 100px">
						<table class="table table-hover  pl-2 pr-2">
							<thead>
								<tr>
									<th scope="col">Name</th>
									<th scope="col">Disk type</th>
									<th scope="col">Capacity</th>
									<th scope="col">VM</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="d in organizacija.diskovi">
									<td>{{d.ime}}</td>
									<td>{{d.tip}}</td>
									<td>{{d.kapacitet}}</td>
									<td>{{d.virtuelnaMasina}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="card" style="height: 100%">
			<div class="card-header">
				<big>Edit organization</big>
			</div>
			<div class="card-body col-md-12" style="float: right; height: 100%">
				<form method="POST" ref="editOrganization" v-on:submit="editOrganization">
					<div class="form-group col-md-12">
						<label>Name</label> <input type="text"
							class="form-control" id="name" v-model="name" name="name" placeholder="Name">
					</div>
					<div class="form-group col-md-12">
						<label>Description</label> <input type="text"
							class="form-control" id="desc" v-model="desc" name="desc" placeholder="Description">
					</div>
					<div class="col-md-12">
						<label>Logo</label>
						<div class="input-group mb-3">
						  <div class="custom-file">
						    <input type="file" accept="image/*" class="custom-file-input" name="i_file" id="i_file" onchange="document.getElementById('lab').innerHTML='Selected'">
						    <label class="custom-file-label" for="i_file" id="lab" >Choose</label>
						  </div>
						</div>
					</div>
					<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 pr-0" role="alert">
			  			<strong>Error!</strong>Organization already exists!
			  		</div>
			  		
					<div class="align-items-center col-md-12 mt-3">
						<button type="submit" class="btn btn-primary btn-lg btn-block" v-on:click="editOrganization()">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	`,
	methods: {
		editOrganization: function() {
			event.preventDefault();
			if(this.korisnik.uloga != "KORISNIK"){
				if(this.name == null || this.name == "") {
					document.getElementById("name").classList.add("is-invalid");
					document.getElementById("name").placeholder = "Mandatory!";
				} else {
					document.getElementById("name").classList.remove("is-invalid");
				}
				
				if(this.desc == null || this.desc == "") {
					document.getElementById("desc").classList.add("is-invalid");
					document.getElementById("desc").placeholder = "Mandatory!";
				} else {
					document.getElementById("desc").classList.remove("is-invalid");
				}
				
				this.i_file = document.getElementById("i_file").value;
				if(this.i_file == null || this.i_file == "") {
					document.getElementById("i_file").classList.add("is-invalid");
				} else {
					document.getElementById("i_file").classList.remove("is-invalid");
				}
				
				
			
				if(this.name != "" && this.name != null && this.desc != null && this.desc != "" && this.i_file != null && this.i_file != "") {
					let formData = new FormData(this.$refs.editOrganization)
					axios
					.post('/main/org/editOrganization/' + this.$route.params.org_id, formData)
					.then(response => {
						if(response.data === "SUCCESS") {
							this.success = true;
							this.$router.push('/organization-view/' + this.name);
							location.reload();
						} else{
							this.success = false;
						}
					});
				}
			}
		}
	},
	
	mounted () {
        axios
		    .get('/main/org/getKategorija/' + this.$route.params.cat_id)
		    .then(response => {
		    	this.kategorija = response.data;
		    })
		    .catch(error => {
		    	console.log("403 - Forbidden for USER.");
		    });
        axios
	        .get('/main/org/getOrganizacija/' + this.$route.params.org_id)
	        .then(response => {
	        	this.organizacija = response.data;
	        })
	        .catch(error => {
			    console.log("403 - Forbidden for USER.");
			});
        axios
	        .get('main/session/getUser')
	        .then(response => {
	        	this.korisnik = response.data;
	        });
    },
});