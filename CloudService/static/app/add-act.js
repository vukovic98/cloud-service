Vue.component ("add-act", {
	    data: function() {
	      return {
	        show: false,
	        ime: null,
	        virtuelnaMasina: null,
	        korisnik: '',
	        success: true,
	        dateFrom: null,
	        dateTo: null
	      }
	    },
	template: `
	<div>
		<button type="button" class="btn btn-primary" v-on:click="open()">Add activity</button>
		    <div id="myModal" class="modal-my">
				
			  <!-- Modal content -->
			  <div class="modal-content-my" style="height: 60%;">
			  	
			  
		<div class="col-md-12 order-md-2 mb-4 ">
			<ul class="list-group mb-3">
				<li class="list-group-item justify-content-between lh-condensed mt-4">
					<div>
						<h4 class="my-0 pb-3">New activity</h4>
						<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend col-md-4">
										<div class="input-group-text col-md-12">Date from</div>
									</div>
									<input type="datetime-local" class="form-control" name="dateFrom" id="dateFrom" v-model="dateFrom">
								</div>
							</div>
					</div>

				</li>


				<li class="list-group-item justify-content-between lh-condensed">
						<div>
							<div class="col-auto">
								<div class="input-group mb-2">
									<div class="input-group-prepend col-md-4">
										<div class="input-group-text col-md-12">Date to</div>
									</div>
									<input type="datetime-local" class="form-control" name="dateTo" id="dateTo" v-model="dateTo">
								</div>
							</div>
						</div>
				</li>

				<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  		<strong>Error!</strong> Invalid activity interval!
				</div>

				<div style="margin-top: 3%">
					<button type="button" class="btn btn-primary btn-lg btn-block" v-on:click="check()">Add</button>
				</div>
				<div style="margin-top: 3%">
					<button type="button" class="btn btn-secondary btn-lg btn-block" v-on:click="close()">Exit</button>
				</div>
			</ul>
			</div>    
		</div>
	</div>
  </div>
	`,
	methods: {
		open: function() {
			if(this.korisnik.uloga == "SUPER_ADMIN")
				document.getElementById("myModal").style.display = "block";
		},
		check: function() {
			if(this.korisnik.uloga == "SUPER_ADMIN") {
			if(this.dateFrom == null || this.dateFrom == "") {
				document.getElementById("dateFrom").classList.add("is-invalid");
			} else {
				document.getElementById("dateFrom").classList.remove("is-invalid");
			}
			
			if(this.dateTo == null || this.dateTo == "") {
				document.getElementById("dateTo").classList.add("is-invalid");
			} else {
				document.getElementById("dateTo").classList.remove("is-invalid");
			}
			
			if(this.dateFrom != null && this.dateFrom != "" && this.dateTo != null && this.dateTo != "") {
				axios
				.post('/main/org/addAct/' + this.name, {
					"datumPaljenja":this.dateFrom,
					"datumGasenja":this.dateTo
				})
				.then(response => {
					if(response.data === "SUCCESS") {
						this.success = true;
						this.$router.push('/machine/' + this.name);
						location.reload();
					} else {
						this.success = false;
					}
				});
			}
			}
		},
		close: function() {
			document.getElementById("myModal").style.display = "none";
			document.getElementById("dateFrom").classList.remove("is-invalid");
			document.getElementById("dateTo").classList.remove("is-invalid");
		},
	},
	mounted () {
		 axios
	         .get('main/org/getVM/' + this.$route.params.id)
	         .then(response => {
	       	  this.virtuelnaMasina = response.data;
	       	  this.name = this.virtuelnaMasina.ime;
         });
		 axios
	        .get('main/session/getUser')
	        .then(response => {
	       	 this.korisnik = response.data;
	        });
	},
});