Vue.component ("sidebar", {
	data: function () {
		return {
			korisnik: '',
			ok: null
		}
	},
	template: `
	<div>
			<ul class="list-group mb-3">
				<a href="/#/vmpage" class="badge badge-light">
					<li class="list-group-item justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">Virtual machines</h6>
							<small class="text-muted">See all virtual machines</small>
						</div>
					</li>
				</a>
				<a href="#/drives" class="badge badge-light">
					<li class="list-group-item justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">Drives</h6>
							<small class="text-muted">See all disks</small>
						</div>
					</li>
				</a>
				<a v-if="korisnik.uloga != 'KORISNIK'" href="#/organizations" class="badge badge-light">
					<li class="list-group-item justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">Organizations</h6>
							<small class="text-muted">See all organizations</small>
						</div>
					</li>
				</a>
				<a v-if="korisnik.uloga == 'SUPER_ADMIN'" href="#/category" class="badge badge-light">
					<li class="list-group-item justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">Categories</h6>
							<small class="text-muted">See all organizations</small>
						</div>
					</li>
				</a>
				<a v-if="korisnik.uloga != 'KORISNIK'" href="#/users" class="badge badge-light">
					<li
					class="list-group-item  justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">Users</h6>
							<small class="text-muted">See all users</small>
						</div>
				</li>
				</a>
				<a href="#/user" class="badge badge-light">
					<li
					class="list-group-item  justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">User</h6>
							<small class="text-muted">See profile informations</small>
						</div>
				</li>
				</a>
				<a v-if="korisnik.uloga == 'ADMIN'" href="#/receipt" class="badge badge-light">
					<li
					class="list-group-item  justify-content-between lh-condensed">
						<div>
							<h6 class="my-0">Receipt</h6>
							<small class="text-muted">Get receipt</small>
						</div>
				</li>
				</a>
			</ul>

			<div class="align-self-end">
        <button type="button" style="margin-top: auto;" class="btn btn-outline-danger btn-lg btn-block" v-on:click="logout()">Logout</button>
      </div>
	</div>
	`,
	methods: {
		logout: function() {
			axios
			.get('/main/logout')
			.then(response => (
				this.$router.push('/logout')
			))
		}
	},
	
	mounted () {
        axios
         .get('main/session/getUser')
         .then(response => {
        	 this.korisnik = response.data;
         });
    },
	
});