Vue.component("login", {
	data: function () {
		return {
			email: null,
			password: null,
			success: true,
			validEMAIL: true
		}
	},
	template: `
		<div class="mt-5" id="login">
			
			<div class="row mt-5">
				<div class="col-md-4"></div>
				<div class="col-md-4 self-align-center">
				<form class="form-signin">
					<div class="form-group">
						<label>Email address</label>
						<input type="email" class="form-control" id="email" v-model="email" placeholder="email@example.com">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" class="form-control" id="password" v-model="password" placeholder="Password">
					</div>
					<button type="button" class="btn btn-outline-primary btn-lg btn-block" v-on:click="login()">Log in</button>
				</form>
				<div v-if="!validEMAIL" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  		<strong>Error!</strong> Email form is not valid!.
				</div>
				<div v-if="!success" class="alert alert-warning alert-dismissible fade show mt-3 align-items-center" role="alert">
			  		<strong>Error!</strong> There is no user with these credentials.
				</div>
				<div class="col-md-4"></div>
				</div>
			</div>
			
		</div>
	`,
	methods: {
		checkSuccess: function (resp) {
			if (resp == "SUCCESS") {
				this.success = true;
				this.$router.push('/vmpage');
			} else {
				this.success = false;
			}
		},
		validEmail: function(email) {
		      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		      return re.test(email);
		},
		login: function () {
			if (this.email == "" || this.email == null) {
				document.getElementById("email").placeholder = "Email must be inserted!"
				document.getElementById("email").classList.add("is-invalid");
			} else {
				document.getElementById("email").classList.remove("is-invalid");
			}
			
			if(!this.validEmail(this.email)) {
				this.validEMAIL = false;
				document.getElementById("email").classList.add("is-invalid");
			} else {
				this.validEMAIL = true;
				document.getElementById("email").classList.remove("is-invalid");
			}

			if (this.password == "" || this.password == null) {
				document.getElementById("password").placeholder = "Password must be inserted!"
				document.getElementById("password").classList.add("is-invalid");
			} else {
				document.getElementById("password").classList.remove("is-invalid");
			}

			if (this.email != null && this.password != null && this.email != "" && this.password != "" && this.validEMAIL) {
				axios
					.post('/main/login', { "email": this.email, "password": this.password })
					.then((response => { this.checkSuccess(response.data) }));
			}
		}
	}
});