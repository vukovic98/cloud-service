const Login = {template: '<login></login>'}
const Logout = {template: '<logout></logout>'}
const VMPage = {template: '<vm-page></vm-page>'}
const Machine = {template: '<machine :id="$route.params.id"></machine>'}
const User = {template: '<user></user>'}
const Users = {template: '<users></users>'}
const UserView = {template: '<user-view :user_id="$route.params.user_id"></user-view>'}
const Category = {template: '<category></category>'}
const CategoryView = {template: '<category-view :cat_id="$route.params.cat_id"></category-view>'}
const Organizations = {template: '<organizations></organizations>'}
const OrganizationView = {template: '<organization-view :org_id="$route.params.org_id"></organization-view>'}
const Drives = {template: '<drives></drives>'}
const DriveView = {template: '<drive-view :drive_id="$route.params.drive_id"></drive-view>'}
const Receipt = {template: '<receipt></receipt>'}

const router = new VueRouter({
	mode: 'hash',
	routes: [
		{path: '/', redirect: '/login'},
		{path: '/login', component: Login},
		{path: '/logout', component: Logout},
		{path: '/vmpage', component: VMPage},
		{path: '/machine/:id', component: Machine},
		{path: '/user', component: User},
		{path: '/users', component: Users},
		{path: '/user-view/:user_id', component: UserView},
		{path: '/category', component: Category},
		{path: '/category-view/:cat_id', component: CategoryView},
		{path: '/organizations', component: Organizations},
		{path: '/organization-view/:org_id', component: OrganizationView},
		{path: '/drives', component: Drives},
		{path: '/drive-view/:drive_id', component: DriveView},
		{path: '/receipt', component: Receipt}
	]
});



var app = new Vue({
	router,
	el: '#login'
});
